if [ $# -eq 0 ] || [ $# -gt 2 ]
then
	echo "usage $0 packagename [NONE|DOCS|PROG|ALL] [ENV]"
	exit 0
fi
package=$1
options=$2
prod="40"
if [ $# -gt 2 ]; then prod=$3; fi;
echo "4.2" > me
echo -e "\"Build $1 package\"\tGETDATE()" >> me
echo -e "1\t'devbuild8'\t'f:/prod${prod}build/'\t'bash.exe -c \"builddevpackage.sh ${package} ${options}\"'\t'Y'\t15\t">>me
loadjob me