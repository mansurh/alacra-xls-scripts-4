import yaml
import re
from .colors import colors
from .print import cprint
from .globals import globals


default_yaml_template = """
tasks:
  - runsql:
      manifest: "{sql_manifest}"
      wait: "Replication done?"
"""

def parse_low(yaml_string):
    try:
        doc = yaml.load(yaml_string)
        globals.doc = doc
    except yaml.YAMLError as exc:
        cprint.error("Error in yaml file:", exc)
        raise 

def parse():
    if (globals.args.config_file.startswith('$')):
        yaml_string = default_yaml_template.format(sql_manifest = globals.args.config_file)
        cprint.print('Generating default yaml file:')
        cprint.print(yaml_string)
        parse_low(yaml_string)
    else:
        with open(globals.args.config_file, 'r') as yaml_string:
            cprint.info('Parsing yaml')
            parse_low(yaml_string)
        
def replace_tokens(instr):
    instr = str(instr)
    g = globals.doc.get("global")
    if g:
        p = re.compile('\${([^}]+)}')
        for match in p.finditer(instr):
            m = match.group(1)
            if m in g:
                instr = instr.replace('${'+m+'}', str(g[m]))
    return instr

def get_tasks():
    return globals.doc["tasks"]



