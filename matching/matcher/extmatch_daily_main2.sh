if [ $# != 2 ]
then
    echo "Usage: extmatch_daily_main.sh id error_emaillist"
    echo "Example: extmatch_daily_main.sh 1 administrators@alacra.com"
    exit 1
fi

id=$1
error_emaillist=$2

LOADDIR=${XLSDATA}/extmatcher
SCRIPTDIR=$XLS/src/scripts/matching/matcher
joblog=${LOADDIR}/jobs.log

TODAY=`date +%d%m%Y`

XLSUTILS=$XLS/src/scripts/loading/dba/
. $XLSUTILS/get_registry_value.fn
. $XLSUTILS/check_return_code.fn

db=matcher
user=`get_xls_registry_value ${db} user`
password=`get_xls_registry_value ${db} password`
server=`get_xls_registry_value ${db} server`

echo ${user}
echo ${password}
echo ${server}

c_db=concordance
c_user=`get_xls_registry_value ${c_db} user`
c_password=`get_xls_registry_value ${c_db} password`
c_server=`get_xls_registry_value ${c_db} server`

#----------------------------------------------------------------

get_next_job()
{
start=`date`
echo "${start}: Getting next job"

cd ${LOADDIR}
check_return_code $? "Error changing to load directory ${LOADDIR} - Aborting" 1

sql="UPDATE match_jobs SET jobStarted = GETDATE(), jobStatus = 10 WHERE id = '${id}'"
isql -n -U${user} -P${password} -S${server} -Q"${sql}" -h-1 -n -b
check_return_code $? "Error updating log table for job ${id} - aborting" 1

# select 1 job
# status is in (0 = Pending, 10 = In Progress, 20 = Error)
sql="SET NOCOUNT ON SELECT TOP 1  j.description, j.emaillist, j.id FROM match_jobs j WHERE j.id='${id}'"
isql -U${user} -P${password} -S${server} -Q"${sql}" -o"${joblog}" -h-1 -n -s":" -b -w"1000"
check_return_code $? "Error getting next job - aborting" 1

# remove all new line characters
tr -d '\n' < ${joblog} > ${joblog}.fmt
read line < ${joblog}.fmt

# parse for each parameter

# trimming all preceding and trailing whitespace
description=$(echo "$line" | cut -d":" -f1)
description=$(echo "$description" | sed 's/^[ \t]*//;s/[ \t]*$//')
echo "${description}"
description=$(echo "$description" | sed 's/ /_/g')
echo "${description}"
emaillist=$(echo "$line" | cut -d":" -f2)
emaillist=$(echo "$emaillist" | sed 's/^[ \t]*//;s/[ \t]*$//')
rm -f ${joblog}
rm -f ${joblog}.fmt

# if we managed to pull down a job
if [[ ${id} != '' ]]
then
	outputfile=${LOADDIR}/output${id}.txt
	exceloutputfile=${LOADDIR}/output${id}.zip
	outputfilename=output${id}.txt

	# this is user id for a/c 6619
	userid=118087

	# display job parameters
	echo "Job ${id} parameters"
	echo "Description: '${description}'"
else
	echo "No more jobs"
fi

end=`date`
echo "${end}: Getting next job - completed" 

}

#----------------------------------------------------------------

run_matcher()
{
start=`date`
echo "${start}: Running ExternalMatcher for job ${id}"

echo "C:/Alacra/App/ExternalMatcher/ExternalMatcher.exe ${id}"
C:/Alacra/App/ExternalMatcher/ExternalMatcher.exe "${id}"
check_return_code $? "Error running ExternalMatcher.exe for job ${id} - aborting" 1

end=`date`
echo "${end}: Running ExternalMatcher for job ${id} - completed" 

}

#----------------------------------------------------------------

load_file_to_database()
{
start=`date`
echo "${start}: Load output file to database for job ${id}"


# delete old results from table
echo "Deleting previous results"
sql="delete from match_user_output where id=${id} "
isql -U${user} -P${password} -S${server} -Q"${sql}" -h-1 -n -b -l0
check_return_code $? "Error deleting previous results for job ${id} - aborting" 1

bcp match_user_output in ${outputfile} -b10000 -f ${SCRIPTDIR}/extmatch_output.fmt -S${server} -U${user} -P${password} -e ${outputfile}_${server}.err
check_return_code $? "Error bcping output into match_user_output for job ${id} - aborting" 1

end=`date`
echo "${end}: Load output file to database for job ${id} - completed" 

}

#----------------------------------------------------------------

email_file()
{
start=`date`
echo "${start}: E-mailing output file ${outputfile} to ${emaillist} for job ${id}"

if [[ ${emaillist} != 'NULL' ]]
then
	size=$(du -ks ${exceloutputfile} | cut -f1)
	echo "Size of file is ${size} Kb"
	tmpfile=${LOADDIR}/email${id}.txt

	#sed -i '1iJob ID|User ID|User Name|User Country|User State|User City|User Ticker|User DUNS|Alacra ID|Alacra Name|Alacra Country|Alacra State|Alacra City|Alacra Ticker|Alacra DUNS|Match Type|Name Score|Matched Name|Matched Country|Matched State|Matched City' ${outputfile} 

	echo "Output for job ${id}" > ${tmpfile}
	blat ${tmpfile} -t "${emaillist}" -s "Matching results ${day} ${description}" -attach "${exceloutputfile}"
	check_return_code $? "Error - e-mailing output file ${exceloutputfile} to ${emaillist} for job ${id} - aborting" 1
	
	rm -f ${tmpfile}
else
	echo "E-mail address list is blank, no need to e-mail output file"
fi

end=`date`
echo "${end}: E-mailing output file ${outputfile} to ${emaillist} for job ${id} - completed" 

}

#----------------------------------------------------------------

load_files_to_locker()
{
start=`date`
echo "${start}: Load log and output files to locker for job ${id}"

echo "concAddToLocker.exe \"${outputfile}\" \"text/plain\" \"${userid}\""
locker_file_id=`concAddToLocker.exe "${outputfile}" "text/plain" "${userid}"`
check_return_code $? "Failed to add ${outputfile} to locker server - Aborting" 1

end=`date`
echo "${end}: Load log and output files to locker for job ${id} - completed" 

}


#----------main routines-----------------------------------------

get_next_job

run_matcher

load_file_to_database

load_files_to_locker

email_file

exit 0