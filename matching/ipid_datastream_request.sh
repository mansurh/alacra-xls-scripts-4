# Script to send the request file to Datastream ftp site
# Created 22 November by John Willcox

#!/bin/sh

# Send error message

send_error() 
{
	error=$1
	echo "${error} - exiting" >> $LOG
	mkdir -p datadir
	cd datadir
	errorFile=${DATADIR}/error.txt
	> $errorFile
	echo "${error}" >> $errorFile
	blat $errorFile -t "administrators@alacra.com" -s "Attempt to send request file to Datastream ftp site failed"
	rm $errorFile
}

# Send request file

send_request() 
{
	if [ !-e $DATASTREAM_FILENAME ]
	then
		send_error "${DATASTREAM_FILENAME} not found"
		exit 1
	fi

	echo "Sending data file from working directory to Datastream FTP server" >> $LOG
	> ${FTP_FILENAME}
	echo "user ${DATASTREAM_LOGIN}" >> ${FTP_FILENAME}
	echo "${DATASTREAM_PW}" >> ${FTP_FILENAME}
	echo "cd request" >> ${FTP_FILENAME}
	echo "binary" >> ${FTP_FILENAME}
	echo "prompt" >> ${FTP_FILENAME}
	echo "put ${DATASTREAM_FILENAME}" >> ${FTP_FILENAME}
	echo "quit" >> ${FTP_FILENAME}
	ftp -i -n -s:${FTP_FILENAME} ${DATASTREAM_FTP}
	if [[ $? != 0 ]]
	then
		send_error "Could not ftp file"
		exit 1
	fi
	echo "Sent data file" >> $LOG
	rm ${FTP_FILENAME}
}

SCRIPTDIR="$XLS/src/scripts/matching"
DATADIR="$XLSDATA/matching/datastream"

LOG="${DATADIR}/ipid_datastream_request.log"

> $LOG

DATASTREAM_FTP="uktfmonddlp1.datastream.com"
DATASTREAM_LOGIN="ACR"
DATASTREAM_PW="Falling290"
DATASTREAM_FILENAME="${SCRIPTDIR}/DD4ACR.req"
FTP_FILENAME="datastream.ftp"

send_request
echo "Done" >> $LOG
exit 0