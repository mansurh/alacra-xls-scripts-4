# Script file to generate the IP matching reports for spcred
# 1 = Output filename
# 2 = XLS Server
# 3 = XLS Login
# 4 = XLS Password
if [ $# -lt 4 ]
then
	echo "Usage: ipid_spcred_report.sh reportfile xls_server xls_login xls_password"
	exit 1
fi

REPORTFILE=$1
xlsserver=$2
xlslogin=$3
xlspassword=$4

# Just send concordance a message saying the spcred ipid was rebuilt
timestamp=`date`
echo "SPCRED IPID Successfully rebuilt on ${timestamp}" > ${REPORTFILE}

exit 0



LOADDIR=${XLS}/src/scripts/loading/spcred
TEMPDIR=${XLSDATA}/matching

# Now build an email to send to security matching
blatfile="${TEMPDIR}/ipid_sp.txt"
blatfiletmp1=${blatfile}.tmp1
blatfiletmp2=${blatfile}.tmp2

finalblatfile=${REPORTFILE}

rm -f ${finalblatfile}
rm -f ${blatfile}
rm -f ${blatfiletmp1}
rm -f ${blatfiletmp2}

echo "*** ipid_spcred update info: *** " > ${finalblatfile}
echo "" >> ${finalblatfile}
echo "*** Companies added: ***" >> ${finalblatfile}
echo "" >> ${finalblatfile}
#isql -w 255 -x 255 -U ${xlslogin} -P ${xlspassword} -S ${xlsserver} -r -Q "set nocount on \
#select distinct convert(varchar(12), itck.matchkey) + '|' + itck.name + '|' + icou.val + '|' + ltrim(rtrim(itck.val)) + '|' + igics.val + '|??' \
#from ipid_spcred icou, ipid_spcred itck, ipid_spcred igics \
#where \
#icou.matchkey = itck.matchkey and \
#itck.matchkey = igics.matchkey and \
#icou.val_type = 'cou' and \
#itck.val_type = 'ticker' and \
#igics.val_type = 'gics' and \
#convert(varchar(12), itck.matchkey) not in (select distinct sourcekey from company_map where source=232)" > ${blatfiletmp1}

#tail +9 < ${blatfiletmp1} | sed -f ${LOADDIR}/ipid_spcred.sed > ${blatfiletmp2}

#while read -r line
#do
#
#    line=${line%% }
#
#    if [ "${line}" != '' ]
#    then
#
#        print "${line}"
#
#    fi
#
#done < ${blatfiletmp2} >> ${finalblatfile}

bcp "select distinct convert(varchar(12), itck.matchkey), itck.name, icou.val, ltrim(rtrim(itck.val)), igics.val from ipid_spcred icou, ipid_spcred itck, ipid_spcred igics where icou.matchkey = itck.matchkey and itck.matchkey = igics.matchkey and icou.val_type = 'cou' and itck.val_type = 'ticker' and igics.val_type = 'gics' and convert(varchar(12), itck.matchkey) not in (select distinct sourcekey from company_map where source=232)" queryout ${blatfiletmp1} -U ${xls_logon} -P ${xls_password} -S ${xls_server} -c -t"|"
if [ $? -ne 0 ]
then
	echo "error in bcp out ADDED companies, exiting..."
	exit 1
fi
cat ${blatfiletmp1} >> ${finalblatfile}

echo "" >> ${finalblatfile}
echo "*** Companies deleted: ***" >> ${finalblatfile}
echo "" >> ${finalblatfile}
#isql -w 255 -x 255 -U ${xlslogin} -P ${xlspassword} -S ${xlsserver} -s"|" -r -x 255 -w 255 -Q "set nocount on select cmp.id, cmp.name, cmp.city, cmp.state_prov, cmp.url, cou.name, cou.iso3166_A2, exch.abbreviation, exch.name \
#from company_map map, company cmp, security sec, country cou, exchange exch \
#where \
#	map.source = 232 and \
#	map.xlsid = cmp.id and \
#	cmp.id = sec.issuer and \
#	cmp.country = cou.id and \
#	sec.exchange = exch.id and \ 
#map.sourcekey not in (select distinct matchkey from ipid_spcred) and '' != isnull(sourcekey, '')" > ${blatfiletmp1}
#tail +5 < ${blatfiletmp1} | sed -f ${LOADDIR}/ipid_spcred.sed > ${blatfiletmp2}

#while read -r line
#do
#
#    line=${line%% }
#
#    if [ "${line}" != '' ]
#    then
#
#        print "${line}"
#
#    fi
#
#done < ${blatfiletmp2} >> ${finalblatfile}

bcp "select cmp.id, cmp.name, cmp.city, cmp.state_prov, cmp.url, cou.name, cou.iso3166_A2, exch.abbreviation, exch.name from company_map map, company cmp, security sec, country cou, exchange exch where map.source = 232 and map.xlsid = cmp.id and cmp.id = sec.issuer and cmp.country = cou.id and sec.exchange = exch.id and map.sourcekey not in (select distinct matchkey from ipid_spcred) and '' != isnull(sourcekey, '')" queryout ${blatfiletmp2} -U ${xls_logon} -P ${xls_password} -S ${xls_server} -c -t"|"
if [ $? -ne 0 ]
then
	echo "error in bcp out DELETED companies, exiting..."
	exit 1
fi
cat ${blatfiletmp2} >> ${finalblatfile}

exit 0
