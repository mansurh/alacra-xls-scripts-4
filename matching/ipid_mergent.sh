# Script file to build the IP matching table for Investext
# 1 = IPID Server
# 2 = IPID Database
# 3 = IPID Login
# 4 = IPID Password
# 5 = MERGENT Server
# 6 = MERGENT Login
# 7 = MERGENT Password
ARGS=7
if [ $# -ne $ARGS ]
then
	echo "Usage: ipid_mergent.sh ipid_server ipid_database ipid_login ipid_password mergent_server mergent_login mergent_password"
	exit 1
fi

IPIDserver=$1
IPIDdatabase=$2
IPIDlogin=$3
IPIDpassword=$4
mergentserver=$5
mergentlogin=$6
mergentpassword=$7

# Name of the temp file to use
TMPFILE1=ipid_mergent1.tmp
TMPFILE2=ipid_mergent2.tmp

# Name of the table to use
TABLENAME=ipid_mergent

# Name of the format file to use
FORMAT_FILE=$XLS/src/scripts/matching/ipid_mergent.fmt

# Step 1 - remove any old temp files
rm -f ${TMPFILE1} ${TMPFILE2}

# Step 2 - select the mergent data into a temporary file
isql -S${mergentserver} -U${mergentlogin} -P${mergentpassword} -s"|" -n -w500 -h-1 >${TMPFILE1} << HERE
SET NOCOUNT ON
select 
	i.EntityReference, i2.LegalName, c.Description "Country", i.CIK, i.CommonTicker, e.Description "Exchange", i.CommonCusip, i.City, i.State, i.Zipcode
from
	identities i, entity i2, countries c, exchanges e
where
	i.CountryCode *= c.CountryCode
	and
	i.CommonExchange *= Exchange
	and 
	i.EntityReference = i2.EntityReference
HERE

# Step 3 - post-process the temp file
sed -e "s/  *|/|/g;s/  *$//g;s/^NULL|/|/;s/|NULL|/||/g;s/|NULL$/|/" < ${TMPFILE1} > ${TMPFILE2}

# Step 4 - drop the old id table - don't check for error
# as it may not exist
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
drop table ${TABLENAME}
go
HERE

# Step 5 - create the new table
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
create table ${TABLENAME} (
	EntityReference varchar(20) NULL,
	LegalName varchar(132) NULL,
	Country varchar(35) NULL,
	CIK varchar(10) NULL,
	CommonTicker varchar(8) NULL,
	Exchange varchar(30) NULL,
	CommonCusip varchar(8) NULL,
	City varchar(50) NULL,
	State varchar(45) NULL,
	Zipcode varchar(20) NULL
)
GO
create index ${TABLENAME}_01 on ${TABLENAME}
	(EntityReference)
GO
create index ${TABLENAME}_02 on ${TABLENAME}
	(LegalName)
GO
create index ${TABLENAME}_03 on ${TABLENAME}
	(Country)
GO
create index ${TABLENAME}_04 on ${TABLENAME}
	(CIK)
GO
create index ${TABLENAME}_05 on ${TABLENAME}
	(CommonTicker)
GO
create index ${TABLENAME}_06 on ${TABLENAME}
	(Exchange)
GO
create index ${TABLENAME}_07 on ${TABLENAME}
	(CommonCusip)
GO
create index ${TABLENAME}_08 on ${TABLENAME}
	(City)
GO
create index ${TABLENAME}_09 on ${TABLENAME}
	(State)
GO
create index ${TABLENAME}_10 on ${TABLENAME}
	(Zipcode)
GO
HERE
if [ $? -ne 0 ]
then
	echo "Error creating table, exiting"
	exit 1
fi

# Step 6 - bcp in the select results
bcp ${TABLENAME} in ${TMPFILE2} /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} /f${FORMAT_FILE} /b1000 
if [ $? -ne 0 ]
then
	echo "Error in BCP, exiting"
	exit 1
fi

# Step 7 - clean up
#rm -f ${TMPFILE1} ${TMPFILE2}
