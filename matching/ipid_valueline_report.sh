# Script file to generate the IP matching reports for valueline
# 1 = Output filename
# 2 = XLS Server
# 3 = XLS Login
# 4 = XLS Password
if [ $# -lt 4 ]
then
	echo "Usage: ipid_valueline_report.sh reportfile xls_server xls_login xls_password"
	exit 1
fi

REPORTFILE=$1
xlsserver=$2
xlslogin=$3
xlspassword=$4

LOADDIR=${XLS}/src/scripts/loading/valueline
TEMPDIR=${XLSDATA}/matching

# Now build an email to send to company matching
blatfile="${TEMPDIR}/ipid_valueline.txt"
blatfiletmp1=${blatfile}.tmp1
blatfiletmp2=${blatfile}.tmp2

finalblatfile=${REPORTFILE}
rm -f ${finalblatfile}
rm -f ${blatfile}
rm -f ${blatfiletmp1}
rm -f ${blatfiletmp2}

echo "*** ipid_valueline update info: *** " > ${finalblatfile}
echo "" >> ${finalblatfile}
echo "*** Companies added: ***" >> ${finalblatfile}
echo "" >> ${finalblatfile}
#isql -U ${xlslogin} -P ${xlspassword} -S ${xlsserver} -r -Q "set nocount on select distinct matchkey + '|' + name + '|??' from ipid_valueline where matchkey not in (select distinct sourcekey from company_map where source=9958)" > ${blatfiletmp1}
#tail +9 < ${blatfiletmp1} | sed -f ${LOADDIR}/ipid_valueline.sed > ${blatfiletmp2}

#while read -r line
#do
#
#    line=${line%% }
#
#    if [ "${line}" != '' ]
#    then
#
#        print "${line}"
#
#    fi
#
#done < ${blatfiletmp2} >> ${finalblatfile}

bcp "select distinct matchkey, name from ipid_valueline where matchkey not in (select distinct sourcekey from company_map where source=9958)" queryout ${blatfiletmp1} -U ${xls_logon} -P ${xls_password} -S ${xls_server} -c -t"|"
if [ $? -ne 0 ]
then
	echo "error in bcp out ADDED companies, exiting..."
	exit 1
fi
cat ${blatfiletmp1} >> ${finalblatfile}

echo "" >> ${finalblatfile}
echo "*** Companies deleted: ***" >> ${finalblatfile}
echo "" >> ${finalblatfile}
#isql -U ${xlslogin} -P ${xlspassword} -S ${xlsserver} -s"|" -r -x 255 -w 255 -Q "set nocount on select distinct m.sourcekey + '|' + convert(varchar(255), m.xlsid) + '|' + c.name + '|' + cou.name + '|' + s.cusipcins + '|' + ltrim(rtrim(s.ticker)) + '|' + e.name from company_map m, company c, country cou, security s, exchange e where source=9958 and c.id=m.xlsid and cou.id=c.country and s.cusipcins=m.sourcekey and s.exchange=e.id and sourcekey not in (select distinct matchkey from ipid_valueline) and 0!=isnull(sourcekey, 0)" > ${blatfiletmp1}
#tail +5 < ${blatfiletmp1} | sed -f ${LOADDIR}/ipid_valueline.sed > ${blatfiletmp2}

#while read -r line
#do
#
#    line=${line%% }
#
#    if [ "${line}" != '' ]
#    then
#
#        print "${line}"
#
#    fi
#
#done < ${blatfiletmp2} >> ${finalblatfile}

bcp "select distinct m.sourcekey, convert(varchar(255), m.xlsid), c.name, cou.name, s.cusipcins, ltrim(rtrim(s.ticker)), e.name from company_map m, company c, country cou, security s, exchange e where source=9958 and c.id=m.xlsid and cou.id=c.country and s.cusipcins=m.sourcekey and s.exchange=e.id and sourcekey not in (select distinct matchkey from ipid_valueline) and 0!=isnull(sourcekey, 0)" queryout ${blatfiletmp2} -U ${xls_logon} -P ${xls_password} -S ${xls_server} -c -t"|"
if [ $? -ne 0 ]
then
	echo "error in bcp out DELETED companies, exiting..."
	exit 1
fi
cat ${blatfiletmp2} >> ${finalblatfile}

exit 0
