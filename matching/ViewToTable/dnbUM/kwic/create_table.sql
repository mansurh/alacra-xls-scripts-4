IF (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
				 WHERE
                 TABLE_NAME = 'ipid_dnbUM_kwic' and TABLE_TYPE = 'BASE TABLE'))
BEGIN
	drop table ipid_dnbUM_kwic
END

CREATE TABLE [dbo].[ipid_dnbUM_kwic](
	[dunsNo] [int] NOT NULL,
	[value] [varchar](128) NOT NULL
) ON [PRIMARY]

GO

