--IPID_ONESOURCE

if Exists(SELECT * FROM sys.objects WHERE  object_id = OBJECT_ID(N'dump_ipid_data_kwic') AND type IN ( N'P', N'PC' ))
BEGIN
	drop procedure dump_ipid_data_kwic
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE dump_ipid_data_kwic

AS
BEGIN
	SET NOCOUNT ON;

	select matchkey,name from onesource_kwic_load

END


