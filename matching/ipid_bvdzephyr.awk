BEGIN {
	FS="\t";
	prevco="";
	prevname="";
	primsic="";

	cur_id="";
	cur_name="";
	cur_sedol="";
	cur_country="";
	cur_primsic="";
	cur_allsic="";
}
{
	if (NR > 1) {
		cur_id=$1;
		cur_name=$2;
		cur_sedol=$3;
		cur_country=$4;
		cur_allsic=$5;
		cur_primsic=$6;

	    gsub("\\|"," ", cur_name);
	    gsub("\\|"," ", cur_sedol);
	    gsub("\\|"," ", cur_country);
	    gsub("\\|"," ", cur_primsic);
	    gsub("\\|"," ", cur_allsic);


		if (cur_name != "") {
			primsic="";

			print cur_id "|" cur_name "|" cur_sedol "|" cur_country >> "zcomp.dat"

			if (cur_primsic != "") {
				print cur_id "|" cur_name "|" cur_primsic "|1" >> "zsic.dat"
				primsic = cur_primsic;
			}

			if (cur_allsic != ""  &&  cur_allsic != cur_primsic)
				print cur_id "|" cur_name "|" cur_allsic "|0" >> "zsic.dat"

			prevco = cur_id;
			prevname = cur_name;
		}
		else
		{
			if (cur_allsic != ""  &&  cur_allsic != primsic)
				print prevco "|" prevname "|" cur_allsic "|0" >> "zsic.dat"

			if (cur_primsic != "") {
				print prevco "|" prevname "|" cur_primsic "|1" >> "zsic.dat"
				primsic = cur_primsic;
			}
		}
	}
}
