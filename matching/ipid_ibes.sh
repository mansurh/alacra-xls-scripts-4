# Script file to build the IP matching table for Investext
# 1 = IPIDServer
# 2 = IPIDDatabase
# 3 = IPIDLogin
# 4 = IPIDPassword
# 5 = IBES Server
# 6 = IBES Login
# 7 = IBES Password
ARGS=7
if [ $# -ne $ARGS ]
then
	echo "Usage: ipid_ibes.sh ipid_server ipid_database ipid_login ipid_password ibes_server ibes_login ibes_password"
	exit 1
fi

IPIDserver=$1
IPIDdatabase=$2
IPIDlogin=$3
IPIDpassword=$4
ibesserver=$5
ibeslogin=$6
ibespassword=$7

# Name of the temp file to use
TMPFILE1=ipid_ibes1.tmp
TMPFILE2=ipid_ibes2.tmp

# Name of the table to use
TABLENAME=ipid_ibes

# Name of the format file to use
FORMAT_FILE=$XLS/src/scripts/matching/ipid_ibes.fmt

# Step 1 - remove any old temp files
rm -f ${TMPFILE1} ${TMPFILE2}

# Step 2 - select the ibes data into a temporary file
# We are only trying to match companies which have a FY1 estimate
# as indicated by their presence in the supplemental table.
isql -S${ibesserver} -U${ibeslogin} -P${ibespassword} -s"|" -n -w500 -h-1 >${TMPFILE1} << HERE
SET NOCOUNT ON          
select 
i.ticker, i.name, i.homeMarketCode, i.ibescusip, 
CASE WHEN SUBSTRING(i.ticker,1,1) = "@" then c.name ELSE 
CASE s.country WHEN 'NC' then "Canada" else 'United States' end 
END "country", 
s.instrumentType 
from 
ident i, country c, supplemental s
where
SUBSTRING(i.ibescusip,1,2) *= c.code and i.ticker=s.ticker 
and i.ticker in (
select i.ticker from ibes..ident i
where hasEPS='Y' or hasEPS='Y' or hasCPS='Y' or hasDPS='Y' or hasBPS='Y' or hasEBG='Y' or hasFFO='Y' or hasSAL='Y' or hasPRE='Y' or hasOPR='Y' or hasEBI='Y' or hasEBT='Y' or hasNET='Y' or hasNAV='Y' or hasROA='Y' or hasROE='Y'
union
select i.ticker from ibes..ident i
where hasEPSPAR='Y' or hasEPSPAR='Y' or hasCPSPAR='Y' or hasDPSPAR='Y' or hasBPSPAR='Y' or hasEBGPAR='Y' or hasFFOPAR='Y' or hasSALPAR='Y' or hasPREPAR='Y' or hasOPRPAR='Y' or hasEBIPAR='Y' or hasEBTPAR='Y' or hasNETPAR='Y' or hasNAVPAR='Y' or hasROAPAR='Y' or hasROEPAR='Y' 
)
HERE

# Step 3 - post-process the temp file
sed -e "s/  *|/|/g;s/  *$//g" < ${TMPFILE1} > ${TMPFILE2}

# Step 4 - drop the old id table - don't check for error
# as it may not exist
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
drop table ${TABLENAME}
go
HERE

# Step 5 - create the new table
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
create table ${TABLENAME} (
	ticker varchar(6),
	name varchar(40),
	homeMarketCode varchar(8),
	ibescusip varchar(8),
	country varchar(32),
	type char(1)
)
GO
create index ${TABLENAME}_01 on ${TABLENAME}
	(ticker)
GO
create index ${TABLENAME}_02 on ${TABLENAME}
	(name)
GO
create index ${TABLENAME}_03 on ${TABLENAME}
	(homeMarketCode)
GO
create index ${TABLENAME}_04 on ${TABLENAME}
	(ibescusip)
GO
create index ${TABLENAME}_05 on ${TABLENAME}
	(country)
GO
HERE
if [ $? -ne 0 ]
then
	echo "Error creating table, exiting"
	exit 1
fi

# Step 6 - bcp in the select results
bcp ${TABLENAME} in ${TMPFILE2} /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} /f${FORMAT_FILE} /b100
if [ $? -ne 0 ]
then
	echo "Error in BCP, exiting"
	exit 1
fi

# Step 7 - clean up
#rm -f ${TMPFILE1} ${TMPFILE2}
