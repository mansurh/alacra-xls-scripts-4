# Script file to build the IP matching table for Investext
# 1 = XLS Server
# 2 = XLS Login
# 3 = XLS Password
# 4 = BID Server
# 5 = BID Login
# 6 = BID Password
if [ $# -lt 6 ]
then
	echo "Usage: ipid_people.sh xls_server xls_login xls_password bid_server bid_login bid_password"
	exit 1
fi

xlsserver=$1
xlslogin=$2
xlspassword=$3
ipserver=$4
iplogin=$5
ippassword=$6

ipname=people

# Name of the temp file to use
TMPFILE1=ipid_${ipname}1.tmp
TMPFILE2=ipid_${ipname}2.tmp

# Name of the table to use
TABLENAME=ipid_${ipname}

# Name of the format file to use
FORMAT_FILE=ipid_${ipname}.fmt

# Step 1 - remove any old temp files
rm -f ${TMPFILE1} ${TMPFILE2}

# Step 2 - select the ${ipname} data into a temporary file
isql -S${ipserver} -U${iplogin} -P${ippassword} -s"|" -w500 -n -h-1 >${TMPFILE1} << HERE
SET NOCOUNT ON
select b.repno matchkey, b.coname name, b.cusip, b.ticker, b.exch exchange,b.city city, 
b.state state, b.country, webadd url  from business b
HERE

# Step 3 - post-process the temp file
sed -f ${XLS}/src/scripts/matching/match.sed < ${TMPFILE1} > ${TMPFILE2}

# Step 4 - drop the old id table - don't check for error
# as it may not exist
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
drop table ${TABLENAME}
go
HERE

# Step 5 - create the new table
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
create table ${TABLENAME} (
	matchkey varchar(5) NULL,
        name varchar(60) NULL,
	cusip varchar(20) NULL,
	ticker varchar(20) NULL,
	exchange varchar(60) NULL,
	city varchar(60) NULL,
	state varchar(6) NULL,
	country varchar(20) NULL,
	url varchar(200) null
)
GO
create index ${TABLENAME}_01 on ${TABLENAME}
	(matchkey)
GO
create index ${TABLENAME}_02 on ${TABLENAME}
	(cusip)
GO
create index ${TABLENAME}_03 on ${TABLENAME}
	(ticker)
GO
create index ${TABLENAME}_04 on ${TABLENAME}
	(name)
GO
create index ${TABLENAME}_05 on ${TABLENAME}
	(exchange)
GO
create index ${TABLENAME}_06 on ${TABLENAME}
	(city)
GO
create index ${TABLENAME}_07 on ${TABLENAME}
	(state)
GO

create index ${TABLENAME}_09 on ${TABLENAME}
	(country)
GO

HERE
if [ $? -ne 0 ]
then
	echo "Error creating table, exiting"
	exit 1
fi

# Step 6 - bcp in the select results
bcp ${TABLENAME} in ${TMPFILE2} -S ${xlsserver} -U ${xlslogin} -P ${xlspassword} -f ${XLS}/src/scripts/matching/${FORMAT_FILE} -b 100
if [ $? -ne 0 ]
then
	echo "Error in BCP, exiting"
	exit 1
fi


# Step 9 - clean up
#rm -f ${TMPFILE1} ${TMPFILE2}
