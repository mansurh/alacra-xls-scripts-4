#!c:/perl/bin/perl
use LWP::Simple;
use XML::Parser;

die "Usage: indexcheck.pl sitemapindexurl sqlfile"
    unless (@ARGV == 2);

my $indexurl=$ARGV[0];
my $sqlfile=$ARGV[1];
my $tempfile="index.xml";

my $SQLFILE;
my $in_loc=0;
my $loc_url="";

system("rm -f $tempfile");

die "Unable to open file for SQL output: $sqlfile"
    unless open(SQLFILE, ">${sqlfile}");

$return=getstore($indexurl, $tempfile);

die "Unable to retrieve sitemapindex file (HTTP err $return)"
    unless ($return == 200);


my $parser = new XML::Parser(ErrorContext => 2,
                             ProtocolEncoding => "UTF-8",
                             Handlers => {Start => \&start_handler,
                                          End => \&end_handler,
                                          Char => \&char_handler}
                             );

$parser->parsefile($tempfile);
close(SQLFILE);

sub start_handler
{
  my $p = shift;
  my $el = shift;
  
  if ($el eq "loc") {
    $in_loc = 1;
    $loc_url="";
  } else {
  }
}

sub end_handler
{
  my $p = shift;
  my $el = shift;

  if ($el eq "loc") {
    $in_loc = 0;
    $rc = system("perl sitemapcheck.pl \"$loc_url\"");
    if ($rc != 0) {
      $sitemap = $loc_url;
      $sitemap =~ s!.+/([\w]+_[\w]+_[\w\d]+_[\d]+).smx(.+)?!$1!g;
      print "$sitemap\n";
      @parts = split(/_/, $sitemap);

      if (@parts == 4) {
        $partnum = $parts[3];
        $partnum =~ s/^0+//g;
        print SQLFILE "update sitemaps set disable=1 where dbase='$parts[0]' and regkey='$parts[1]' and server='$parts[2]' and part=$partnum\n";
      }
    }

    $mdfile = $loc_url;
    $mdfile =~ s/\.smx/\.gpx/g;
    # $rc = system("perl metadatacheck.pl \"$mdfile\"");
  }
}

sub char_handler
{
  my ($p, $data) = @_;
  
  if ($in_loc == 1) {
    $loc_url .= $data;
  } 
}
