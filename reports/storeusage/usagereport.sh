XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

# Parse the command line arguments
#	1 - database
if [ $# -lt 1 ]
then
	echo "Usage: $0 database"
	exit 1
fi

database=$1
# Save the runstring parameters
logon=`get_xls_registry_value ${database} User`
password=`get_xls_registry_value ${database} Password`
dataserver=`get_xls_registry_value ${database} Server`
emailgrp=storeusagereport@alacra.com

mkdir -p $XLSDATA/reports
cd $XLSDATA/reports
reportfile=storeusagereport`date +%y%m%d`.txt

isql /U${logon} /P${password} /S${dataserver} /b /n /w 500 < $XLS/src/scripts/reports/storeusage/usagereport.sql > ${reportfile}
if [ $? -ne 0 ]
then
	exit 1
fi

blat ${reportfile} -t "${emailgrp}" -s "Daily Store Usage Report"

