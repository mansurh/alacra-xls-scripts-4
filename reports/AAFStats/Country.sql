set nocount on
set transaction isolation level read uncommitted

/* Statistics by Country */
select i.Country, g.name as Name, COUNT(*) as [Count]
FROM ipid_cici i
JOIN country g ON i.Country = g.iso3166_a2
GROUP BY i.Country, g.name
ORDER BY COUNT(*) desc
