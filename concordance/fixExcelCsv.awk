BEGIN { FS="\t"; OFS="\t"; }
{
    # sometimes we only get 5 columns back
    if ( NF == 5) {
        print $0 "\t\t\t\t\t\t";
    } else if ( NF > 11) {
      print $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11
    } else {
      print;
    }
}
