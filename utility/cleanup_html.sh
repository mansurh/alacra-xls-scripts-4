XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/check_arguments.fn
. $XLSUTILS/check_return_code.fn

check_arguments $# 3 "usage: cleanup_html.sh <directory> <infile> <outfile> <xslfile [optional]>" 1

dir=$1
in=$2
out=$3
xslfile=$XLS/src/scripts/utility/toc.xsl
if [ $# = 4 ]
then

    xslfile=$4

fi

cd ${dir}
check_return_code $? "could not change to directory ${dir}" 1

tidy -asxhtml -numeric -quiet ${in} > output.tmp 2>output.err
retval=$?
# echo "html tidy retval is ${retval}"
if [ ${retval} != 1 ]
then

    echo "tidy.exe failed on input file ${in} (retval is ${retval})"
    exit 1

fi

echo "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>" > tmp.xml
sed "s/<!DOCTYPE.*//g" output.tmp | sed "s/.*\.dtd\">//g" | sed "s/<html .*>/<html>/" >> tmp.xml

cmptrans.exe tmp.xml ${xslfile} > ${out}
check_return_code $? "xsl transform failed while trying to create ${out}" 1

rm output.tmp
rm output.err
rm tmp.xml
