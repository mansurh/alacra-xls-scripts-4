SCRIPTDIR=$XLS/src/scripts/utility
. ${SCRIPTDIR}/services.fn

today=`date +%Y%m%d`
logfile="alertsrestart${today}.log"
date >> $logfile
stop_service "XLS Alert Services" >> $logfile 2>&1
sleep 3
kill_process alertservice.exe  >> $logfile 2>&1
start_service "XLS Alert Services"  >> $logfile 2>&1
date >> $logfile
