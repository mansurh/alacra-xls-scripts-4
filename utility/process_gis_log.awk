BEGIN{
  FS="\t"
  OFS="\t"
}
{
  split($3,arr, " ")
  split(arr[16], arr2, "\\")
  if ( arr2[2] == "invindex" ) {
    invindex[arr2[3]]++
    print arr2[3], substr(arr[8],1,length(arr[8])-1) >> "temp.out"
  } else {
    invindex[arr2[2]]++
    print arr2[2], substr(arr[8],1,length(arr[8])-1) >> "temp.out"
  }

}

END {
  if (NR != 0) {
    print "Summary of requests:"
    for (a in invindex)
      print a, invindex[a]
    print "--------------------"
  }
}
