DECLARE @startdate datetime
DECLARE @enddate datetime

select @startdate='September 1, 2002'
select @enddate='October 1, 2002'

print 'DMI Usage'
select 
	p.company 'Company',
	p.city 'City',
	s.name 'State',
	c.name 'Country',
	p.last_name 'User Name',
	u.description 'Description',
	u.access_time 'Date',
	u.list_price 'List Price',
	u.price 'Price'
from 
	usage u JOIN users p ON u.userid = p.id and p.commence <= u.access_time
	LEFT JOIN state_prov s ON p.state_prov = s.id
	LEFT JOIN country c ON p.country = c.id
where
	u.ip = 137
	and
	p.demo_flag is null
	and
	u.access_time >= @startdate
	and
	u.access_time < @enddate
	and
	(
		u.description like 'D&B In-depth%'
		or
		u.description like 'D&B Standard%'
		or
		u.description like 'D&B Summary%'
	)

print 'Report Usage'
select 
	p.company 'Company',
	p.city 'City',
	s.name 'State', 
	c.name 'Country',
	p.last_name 'User Name',
	u.description 'Description', 
	u.access_time 'Date',
	u.list_price 'List Price', 
	u.price 'Price'
from 
	usage u INNER JOIN users p ON u.userid = p.id and p.commence <= u.access_time
	LEFT JOIN state_prov s ON p.state_prov = s.id
	LEFT JOIN country c ON p.country = c.id
where
	u.ip = 137
	and
	p.demo_flag is null
	and
	u.access_time >= @startdate
	and
	u.access_time < @enddate
	and 
	(
		u.description not like 'D&B In-depth%'
		and
		u.description not like 'D&B Standard%'
		and
		u.description not like 'D&B Summary%'
	)
