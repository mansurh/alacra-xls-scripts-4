/*** BCG STUFF ***/

select "BCG - Forrester Usage"
select 
	i.ipcode, p.account, p.login, u.project,u.project2,u.project3,u.project4, u.project5,u.access_time,u.description,u.list_price 
from 
	usage u, users p, ip i
where 
	userid in (select id from users where account in (select id from account where name like "%bcg%" or name like "%Boston Consulting%") and demo_flag is NULL)
	and 
	access_time >= "October 1, 2004" 
	and 
	access_time < "November 1, 2004"
	and 
	no_charge_flag is null
	and 
	u.userid=p.id
	and
	u.ip = i.id
	and
	i.ipcode in (
		"FORRT"
	)

order by 
	i.ipcode, p.account, p.login, u.ip, u.access_time asc



