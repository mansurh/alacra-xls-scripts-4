

select "Merrill UK Usage"
select 
	i.ipcode, p.account, p.login, u.project,u.project2,u.project3,u.project4, u.project5,u.access_time,u.description,u.list_price 
from 
	usage u, users p, ip i
where 
	userid in (select id from users where account in (2470) and demo_flag is NULL)
	and 
	access_time >= "October 1, 2002" 
	and 
	access_time < "November 1, 2002"
	and 
	no_charge_flag is null
	and 
	u.userid=p.id
	and
	u.ip = i.id

order by 
	i.ipcode, p.account, p.login, u.ip, u.access_time asc


