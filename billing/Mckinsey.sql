select "McKinsey Usage"

select 
	p.login,u.project,u.access_time,u.description,u.list_price, price 
from 
	usage u, 
	users p 
where 
	ip in (2,29,37,44,45,56,59,61,66,67,68,79,80,83,84,89,90,96,99,102,109,112,126,128,161,214,182,157,187,189,234,238,244,246,272)
	and 
	userid in (select id from users where account in (select id from account where name like "%McKinsey%") and demo_flag is NULL)
	and 
	access_time >= "September 1, 2004" and access_time < "October 1, 2004"
	and 
	no_charge_flag is null
	and 
	u.userid=p.id

order by p.login, u.ip, u.access_time asc
