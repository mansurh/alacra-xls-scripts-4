select  

u.ip 'Database ID',

i.name 'Database Name',

 

--can't use flag table to get credentials as it might not exist for PPV

--COALESCE (SUBSTRING(f.value, CHARINDEX('/', f.value, CHARINDEX('/', f.value)+1)+1, 20),'180682491') 'CapIQ API ID (CIQUserObjectId)',

case when CHARINDEX ( 'UOI:' ,s.ipinfo)>0 then substring (s.ipinfo, charindex('UOI:', s.ipinfo)+4, charindex('|', s.ipinfo, charindex('UOI:', s.ipinfo)) - charindex('UOI:',s.ipinfo) - 4) else '' end 'CapIQ API ID (CIQUserObjectId)',

 

p.account 'Alacra Client Account',

a.name 'Name',

COALESCE (t.name,'') 'Alacra Client Type',

 

case

when CHARINDEX('|', s.ipinfo) > 0

then

   SUBSTRING(s.ipinfo, CHARINDEX('|', s.ipinfo, CHARINDEX('|', s.ipinfo, CHARINDEX('|', s.ipinfo)+1)+1)+1, 

          CHARINDEX('|', s.ipinfo, CHARINDEX('|', s.ipinfo, CHARINDEX('|', s.ipinfo, CHARINDEX('|', s.ipinfo)+1)+1)+1) - CHARINDEX('|', s.ipinfo, CHARINDEX('|', s.ipinfo, CHARINDEX('|', s.ipinfo)+1)+1) - 1)

else

   ''

end 'CapIQ Contributor ID',

 

u.dataset 'CapIQ Contributor Name',

 

p.login 'Alacra Client User Name (login)',

 

case 

when CHARINDEX('|',s.ipinfo) > 0

then

        SUBSTRING(s.ipinfo,1,CHARINDEX('|',s.ipinfo)-1) 

else

        ''

end 'CapIQ Document ID',

 

u.description 'Headline',

 

CASE

WHEN CHARINDEX('Filing Date:',u.description) > 1 THEN SUBSTRING(u.description,CHARINDEX('Filing Date:',u.description)+12,18) 

ELSE ' '

END

'Filing Date',

 

case 

when CHARINDEX('|',s.ipinfo) > 0

then

        SUBSTRING(s.ipinfo, CHARINDEX('|',s.ipinfo)+1,CHARINDEX('|', s.ipinfo, CHARINDEX('|', s.ipinfo)+1) - CHARINDEX('|',s.ipinfo) - 1) 

else

        ''

end 'Pages Purchased',

 

u.access_time 'Download Date/Time',

u.list_price 'List Price',

u.price 'Alacra Billed Price',

 

CASE

WHEN (u.no_charge_flag is NULL) THEN ''

ELSE 'Y'

END

'Non-Billable',

 

case when CHARINDEX ( 'SM:' ,s.ipinfo)>0 then substring (s.ipinfo, charindex('SM:', s.ipinfo)+3, charindex('|', s.ipinfo, charindex('SM:', s.ipinfo)) - charindex('SM:',s.ipinfo) - 3) else '' end SalesModel

 

from 

usage u JOIN usagespecial s ON u.usageid = s.usage JOIN ip i ON u.ip = i.id

JOIN users p ON u.userid = p.id JOIN account a ON p.account = a.id

--LEFT JOIN accounts_flag f ON p.account = f.accountid

LEFT JOIN dmofirm_type t ON a.dmofirm_type = t.id

where

--f.name='CapitalIQAPI_Credentials'

--and

u.ip in (470)

and

u.access_time >= 'November 1, 2014' and u.access_time < 'December 1, 2014'

/* Eliminate Alacra demo test usage */
and s.ipinfo not like '%214853681%'

 

/* and COALESCE (f.value,'C6UJ9A003M44') in ('C6UJ9A003M42','C6UJ9A003M44') */

 

order by u.access_time asc