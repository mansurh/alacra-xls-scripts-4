select 'PWC Thomson Usage'
select 
	i.name "Dataset", 
	u.access_time "Date of Transaction",
	p.id "User id",
	CASE WHEN p.service=41 THEN 'Alacra Book' ELSE 'Alacra' END "Product",
	p.last_name "User Last Name",
	a.billing_contact "Billing Contact",
	p.company "Account Name",
	a.billing_address1 "Billing Address 1",
	a.billing_address2 "Billing Address 2",
	a.billing_address3 "Billing Address 3",
	a.billing_city "Billing City",
	s.name "Billing State",
	c.name "Billing Country",
	a.billing_email "Billing Email",
	u.project "Project Code",
	u.list_price "Retail Price",
	u.price "Actual Price"
from
	ip i JOIN usage u ON u.ip = i.id JOIN users p ON u.userid = p.id JOIN
	account a ON p.account = a.id LEFT JOIN state_prov s ON
	a.billing_state_prov = s.id LEFT JOIN country c ON a.billing_country = c.id
where
	(a.name like 'Pricewaterhouse%' or a.id in (2649,3474))
	and
	i.id in (68, 83)
	and 
	u.access_time >= 'August 1, 2003'
	and
	u.access_time <= 'September 1, 2003'
	and
	p.demo_flag is null
	and
	u.no_charge_flag is null
	
order by
	i.id, p.id, u.access_time

	
/**********************
update usage set no_charge_flag = 1 where usageid in (
select 
	u.usageid
from
	ip i, usage u, users p, account a, state_prov s, country c
where
	u.ip = i.id
	and
	u.userid = p.id
	and
	p.account = a.id
	and
	a.billing_state_prov *= s.id
	and
	a.billing_country *= c.id
	and
	a.name like "Pricewaterhouse%"
	and
	i.id in (68, 83)
	and 
	u.access_time >= "October 1, 2002"
	and
	u.access_time <= "November 1, 2002"
	and
	p.demo_flag is null
	and
	u.no_charge_flag is null
)
**********************/


