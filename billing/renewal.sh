XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

# Parse the command line arguments
#	1 - database
if [ $# -lt 1 ]
then
	echo "Usage: $0 database"
	exit 1
fi

database=$1
# Save the runstring parameters
logon=`get_xls_registry_value ${database} User`
password=`get_xls_registry_value ${database} Password`
dataserver=`get_xls_registry_value ${database} Server`

#set output file names
OUTPUT='./renewal.txt'

rm -f $OUTPUT

isql /U${logon} /P${password} /S${dataserver} -n /w 500 << EOF > $OUTPUT

use xls
go
set nocount on
go

declare @startdate smalldatetime
declare @enddate smalldatetime

/* Run for current month */
select @startdate = GETDATE()

/* Move to beginning of the month */
select @startdate = DATEADD(day, -1 * (DATEPART(day, @startdate) -1), @startdate)
select @startdate = DATEADD(hour, -1 * DATEPART(hour, @startdate), @startdate)
select @startdate = DATEADD(minute, -1 * DATEPART(minute, @startdate), @startdate)

/* Calculate the end of the period */
select @enddate = DATEADD (month, 3, @startdate)

select "Upcoming renewals for the 3 months beginning", @startdate

PRINT ""

DECLARE @accountID int
DECLARE @accountName varchar(80)
DECLARE @accountExpiration varchar(12)
DECLARE @message varchar(255)
DECLARE accountCursor CURSOR for

select 
	a.id,
	UPPER(a.name),
	CONVERT(varchar(12),max(c.end_date),107)
from	
	charge c LEFT JOIN salesperson s on c.ddl_salesperson = s.id join account a on c.account = a.id
	LEFT JOIN state_prov sp ON a.state_prov = sp.id LEFT JOIN country cy ON a.country = cy.id
group by 
	a.id,
	a.name
having 
	max(c.end_date) >= @startdate  and max(c.end_date) < @enddate
order by max(c.end_date) asc

OPEN accountCursor

FETCH NEXT from accountCursor INTO @accountID, @accountName, @accountExpiration
WHILE (@@FETCH_STATUS <> -1)
BEGIN

	PRINT "===================================================================================================="
	select @message=  @accountName + ".           expires:" +  @accountExpiration
	PRINT @message
	PRINT ""

	select
		CONVERT(varchar(24),a.contact) "Contact", 
		CONVERT(varchar(16),a.city) "City",
		CONVERT(varchar(6),sp.abbreviation) "St",
		CONVERT(varchar(16),cy.name) "Country",
		COALESCE (a.tel_area, a.billing_tel_area) "Area",
		COALESCE (a.tel_number, a.billing_tel_number) "Telephone"
	from	charge c JOIN account a ON c.account = a.id LEFT JOIN state_prov sp ON a.state_prov = sp.id
		LEFT JOIN country cy ON a.country = cy.id
	where	a.id = @accountID
	group by 
		a.contact,
		a.city,
		sp.abbreviation,
		cy.name,
		COALESCE (a.tel_area, a.billing_tel_area),
		COALESCE (a.tel_number, a.billing_tel_number)

	select
		CONVERT(varchar(16),max(s.last_name))"Inside Rep", 
		CONVERT(varchar(16),max(s2.last_name))"Outside Rep"
	from	account a JOIN charge c ON c.account = a.id LEFT JOIN salesperson s ON c.ddl_salesperson = s.id
		LEFT JOIN salesperson s2 ON c.outside_salesperson = s2.id
	where	a.id = @accountID

	PRINT ""

	select 
		CONVERT(varchar(12),c.start_date,107) "Starting",
		CONVERT(varchar(12),c.end_date,107) "Ending",
		CONVERT(varchar(40),c.description) "Description",
		CONVERT(varchar(12),c.amount) "Amount", 
		CONVERT(varchar(20),b.name) "How Billed"
	from 
		charge c,
		billable_type b
	where 
		c.account = @accountID 
		and
		c.how_billed = b.id
	order by start_date asc

	PRINT ""

	select 
		count(*) "Logins in last 3 months"
	from 
		logins l,
		users u
	where
		l.userid = u.id
		and
		u.account = @accountID
		and
		l.time > DATEADD (month, -3, @startdate)

	PRINT ""
	
	FETCH NEXT FROM accountCursor INTO @accountID, @accountName, @accountExpiration
END

GO

DEALLOCATE accountCursor

EOF

# Mail results to Colin, Mike Angle and sales guys
blat $OUTPUT -t allsales@xls.com,cdusaire@xls.com -s "Renewal Report"
