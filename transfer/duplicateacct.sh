# duplicateacct.sh - copies account data from one account to another on the same server
#parse the command line arguments
# 1 = server
# 2 = user
# 3 = passwd
# 4 = old ID
# 5 = new ID

shname=duplicateacct.sh
if [ $# -lt 5 ]
then
    echo "Usage: ${shname} server user passwd oldID newID"
    exit 1
fi

server=${1}
user=${2}
passwd=${3}
oldid=${4}
newid=${5}


allacombos="account/id account_perm/account accounts_flag/accountId accountmenuitemflags/account_id pricing/account pricing_strings/account validation_list_projects/account validation_regexp_projects/Account pbaccountinfo/account"
for c in $allacombos
do
	table="${c%%/*}"
	key="${c##*/}"

	echo "Copy ${table}?"
	read z
	if [ "${z}" = "y" ]
	then
		logfile="duplicateacct_${table}.log"
		if [ -e ${logfile} ]
		then
			rm -f ${logfile}
		fi

		echo "Copying ${table}.."

		# copy over the data from one ID to another
		$XLS/src/scripts/transfer/duplicaterows.sh ${server} ${user} ${passwd} ${table} ${key} ${oldid} ${newid} >> ${logfile} 2>&1
#		$XLS/src/scripts/transfer/duplicaterows.sh ${server} ${user} ${passwd} ${table} ${key} ${oldid} ${newid}
		if [ $? != 0 ]
		then
		    echo "${shname}: Error copying data!"
		    echo "${shname}: Error copying data!" >> ${logfile}
		    exit 1
		fi

		if [ -e ${logfile} ]
		then
			rm -f ${logfile}
		fi
	fi
done

exit 0
