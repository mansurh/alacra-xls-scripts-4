#parse the command line arguments
# 1 = source server
# 2 = source login
# 3 = source passwd
# 4 = real table name
# 5 = temp table name

shname=TruncateAndInsert.sh
if [ $# -lt 5 ]
then
    echo "Usage: ${shname} srcserver srclogin srcpasswd realtablename temptablename"
    exit 1
fi

srcserver=${1}
srcuser=${2}
srcpasswd=${3}
realtablename=${4}
temptablename=${5}

asdf=$(date +%Y%W%H%M%S)
tempfilename=TruncateAndInsert${asdf}.sql

if [ -e ${tempfilename} ]
then
	rm -f ${tempfilename}
fi

echo "TRUNCATE TABLE ${realtablename}" > ${tempfilename}
echo "GO" >> ${tempfilename}
echo "" >> ${tempfilename}
echo "INSERT ${realtablename} SELECT * FROM ${temptablename}" >> ${tempfilename}
echo "GO" >> ${tempfilename}
echo "" >> ${tempfilename}
echo "DECLARE @c1 int" >> ${tempfilename}
echo "DECLARE @c2 int" >> ${tempfilename}
echo "SELECT @c1=COUNT(*) FROM ${realtablename}" >> ${tempfilename}
echo "SELECT @c2=COUNT(*) FROM ${temptablename}" >> ${tempfilename}
echo "IF @c1 = @c2" >> ${tempfilename}
echo "BEGIN" >> ${tempfilename}
echo "PRINT 'Dropping temp table ${temptablename}'" >> ${tempfilename}
echo "DROP TABLE ${temptablename}" >> ${tempfilename}
echo "END" >> ${tempfilename}
echo "ELSE" >> ${tempfilename}
echo "BEGIN" >> ${tempfilename}
echo "PRINT 'Counts do not match!  NOT dropping temp table ${temptablename}'" >> ${tempfilename}
echo "END" >> ${tempfilename}
echo "GO" >> ${tempfilename}


echo "${shname}: Truncating ${realtablename} and loading rows from temp table ${temptablename}"
echo "${shname}: isql -S ${srcserver} -U ${srcuser} -P ${srcpasswd} -d xls < ${tempfilename}"
isql -S ${srcserver} -U ${srcuser} -P ${srcpasswd} -d ${srcdb} < ${tempfilename}
if [ $? != 0 ]
then
	echo "${shname}: Error truncating ${realtablename} and loading rows from temp table ${temptablename}"
	exit 1
fi

rm -f ${tempfilename}

exit 0
