XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/check_bcp_errors.fn

#parse the command line arguments
# 1 = table name
# 2 = destdb
# 3 = dest server
# 4 = dest login
# 5 = dest passwd
# 6 = temp file
# 7 = checktablesize [optional, default=1]

shname=$0
if [ $# -lt 6 ]
then
    echo "Usage: ${shname} desttable destdb destserver destlogin destpasswd tempfile"
    exit 1
fi

desttable=${1}
destdb=${2}
destserver=${3}
destuser=${4}
destpasswd=${5}
tempfile=${6}
checktablesize=1
if [ $# -gt 6 ]; then checktablesize=$7; fi;

tempfilein=${tempfile}.in

echo "${shname}: Loading of ${desttable} table from disk to ${destdb} (${destserver})"

bcp ${destdb}.dbo.${desttable} in ${tempfile} /S${destserver} /U${destuser} /P${destpasswd} /N /CRAW /E /q /b1000 /o${tempfilein} /e${tempfilein}.err 
if [ $? != 0 ]; then echo "${shname}: Error loading destination data from file"; exit 1; fi

cat ${tempfilein} 
check_bcp_errors ${tempfilein} 1 "Failed to bcp ${desttable} table. Aborting" 1

exit 0
