if [ $# -lt 4 ]
then
	echo "usage: $0 srcsrv destsrv destuser destpass"
	exit 1
fi
srcserver=$1
destserver=$2
destuser=$3
destpass=$4

SCRIPTDIR=$XLS/src/scripts/transfer/xlsbackup
echo "dropping triggers"
isql /U${destuser} /P${destpass} /S${destserver} /b < ${SCRIPTDIR}/disable_alaxls_log_triggers.sql
if [ $? -ne 0 ]
then
	echo "error in drop_alaxls_log_triggers.sql, exiting...  "
	exit 1
fi
isql /U${destuser} /P${destpass} /S${destserver} /Q ""
echo "copying data"
isql /U${destuser} /P${destpass} /S${destserver} /b << HERE
set transaction isolation level read uncommitted
truncate table alaxls_log_copy
go
insert alaxls_log_copy select * from ${srcserver}.xls.dbo.alaxls_log
print 'done getting log data'
go
declare @maxtime datetime
select @maxtime=max(updatedate) from alaxls_log_copy
select @maxtime
delete from pbusage where pbusage in (select id from ${srcserver}.xls.dbo.alaxls_log l where l.updatedate <= @maxtime and l.table_name = 'pbusage')
delete from prepbook where prepbookid in (select id from ${srcserver}.xls.dbo.alaxls_log l where l.updatedate <= @maxtime and l.table_name = 'prepbook')
delete from prepbooktoc where prepbookid in (select id from ${srcserver}.xls.dbo.alaxls_log l where l.updatedate <= @maxtime and l.table_name = 'prepbooktoc')
delete from shoppingcart where shoppingcartid in (select id from ${srcserver}.xls.dbo.alaxls_log l where l.updatedate <= @maxtime and l.table_name = 'shoppingcart')
delete from usage where usageid in (select id from ${srcserver}.xls.dbo.alaxls_log l where l.updatedate <= @maxtime and l.table_name = 'usage')
delete from usageSpecial where usage in (select id from ${srcserver}.xls.dbo.alaxls_log l where l.updatedate <= @maxtime and l.table_name = 'usage')
print 'done deleting'
go
declare @maxtime datetime
select @maxtime=max(updatedate) from alaxls_log_copy
select @maxtime
insert pbusage select * from ${srcserver}.xls.dbo.pbusage where pbusage in (select id from ${srcserver}.xls.dbo.alaxls_log where updatedate <= @maxtime and deleted is null and table_name = 'pbusage')
insert prepbook select * from ${srcserver}.xls.dbo.prepbook where prepbookid in (select id from ${srcserver}.xls.dbo.alaxls_log where updatedate <= @maxtime and deleted is null and table_name = 'prepbook')
insert prepbooktoc select * from ${srcserver}.xls.dbo.prepbooktoc where prepbookid in (select id from ${srcserver}.xls.dbo.alaxls_log where updatedate <= @maxtime and deleted is null and table_name = 'prepbooktoc')
insert shoppingcart select * from ${srcserver}.xls.dbo.shoppingcart where shoppingcartid in (select id from ${srcserver}.xls.dbo.alaxls_log where updatedate <= @maxtime and deleted is null and table_name = 'shoppingcart')
insert usage select * from ${srcserver}.xls.dbo.usage where usageid in (select id from ${srcserver}.xls.dbo.alaxls_log where updatedate <= @maxtime and deleted is null and table_name = 'usage')
insert usageSpecial select * from ${srcserver}.xls.dbo.usageSpecial where usage in (select id from ${srcserver}.xls.dbo.alaxls_log where updatedate <= @maxtime and deleted is null and table_name = 'usage')
delete ${srcserver}.xls.dbo.alaxls_log where updatedate <= @maxtime
print 'done inserting, deleted rows from alaxls_log on source server'
GO
delete top(100) from session where valid_thru < getdate() and id not in (select id from session_archive)
while (@@ROWCOUNT = 100) delete top(100) from session where valid_thru < getdate() and id not in (select id from session_archive)
print 'done deleting from session tables'
go
declare @maxtime datetime
select @maxtime = max(time) from logins
select @maxtime
insert logins select * From ${srcserver}.xls.dbo.logins where time > @maxtime
print 'done with logins'
go
declare @maxtime datetime
select @maxtime = max(time) from logins_internal
select @maxtime
insert logins_internal select * From ${srcserver}.xls.dbo.logins_internal where time > @maxtime
print 'done with logins_internal'
go
declare @maxtime datetime
select @maxtime = max(inserted_time) from shoppingcartlog
select @maxtime
insert shoppingcartlog select * From ${srcserver}.xls.dbo.shoppingcartlog where inserted_time > @maxtime
print 'done with shoppingcartlogs'
go
declare @maxtime datetime
select @maxtime = max(timestamp) from usage_ncflog
select @maxtime
insert usage_ncflog select * From ${srcserver}.xls.dbo.usage_ncflog where timestamp > @maxtime
print 'done with usage_ncflog'
go
declare @maxtime datetime
select @maxtime = max(registration_date) from whitepaper_subscribers
select @maxtime
insert whitepaper_subscribers select * From ${srcserver}.xls.dbo.whitepaper_subscribers where registration_date > @maxtime
print 'done with whitepaper_subscribers'
go
declare @maxtime datetime
select @maxtime = max(datetime) from pbfileuploadlog
select @maxtime
insert pbfileuploadlog select * From ${srcserver}.xls.dbo.pbfileuploadlog where datetime > @maxtime
print 'done with pbfileuploadlog'
GO
HERE
if [ $? -ne 0 ]
then
	echo "error in copying incremental data, exiting...  "
	exit 1
fi

echo "recreating triggers"
isql /U${destuser} /P${destpass} /S${destserver} /b < ${SCRIPTDIR}/enable_alaxls_log_triggers.sql
if [ $? -ne 0 ]
then
	echo "error in create_alaxls_log_triggers.sql, exiting...  "
	exit 1
fi
echo "transfer done"
