IF EXISTS (SELECT name FROM sysobjects  
WHERE name = 'shoppingcartlog_nodelupd' AND type = 'TR')  
    drop trigger shoppingcartlog_nodelupd      
IF EXISTS (SELECT name FROM sysobjects  
WHERE name = 'shoppingcart_del' AND type = 'TR')  
     drop trigger shoppingcart_del  
IF EXISTS (SELECT name FROM sysobjects  
WHERE name = 'shoppingcart_insupd' AND type = 'TR')  
    drop trigger shoppingcart_insupd        
IF EXISTS (SELECT name FROM sysobjects  
WHERE name = 'prepbook_del' AND type = 'TR')  
    drop trigger prepbook_del        
IF EXISTS (SELECT name FROM sysobjects  
WHERE name = 'prepbook_insupd' AND type = 'TR')  
    drop trigger prepbook_insupd        
IF EXISTS (SELECT name FROM sysobjects  
WHERE name = 'prepbooktoc_del' AND type = 'TR')  
    drop trigger prepbooktoc_del        
IF EXISTS (SELECT name FROM sysobjects  
WHERE name = 'prepbooktoc_insupd' AND type = 'TR')  
    drop trigger prepbooktoc_insupd        
IF EXISTS (SELECT name FROM sysobjects  
WHERE name = 'pbusage_nodelete' AND type = 'TR')  
    drop trigger pbusage_nodelete            
IF EXISTS (SELECT name FROM sysobjects  
WHERE name = 'pbusage_insupd' AND type = 'TR')  
    drop trigger pbusage_insupd        
IF EXISTS (SELECT name FROM sysobjects  
WHERE name = 'usage_nodelete' AND type = 'TR')  
    drop trigger usage_nodelete      
IF EXISTS (SELECT name FROM sysobjects  
WHERE name = 'usage_insupd' AND type = 'TR')  
    drop trigger usage_insupd      
