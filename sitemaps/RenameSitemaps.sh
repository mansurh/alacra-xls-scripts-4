if [ $# -lt 1 ]
then
	echo "usage: $a sourceserver"
	exit 1
fi

source=$1
usource=`echo ${source} |tr a-z A-Z`
lsource=`echo ${source} |tr A-Z a-z`
for a in `ls *${usource}_???.smx* *${usource}_???.gpx*`
do
	echo "renaming $a to ${a/${usource}/${COMPUTERNAME}}"
	mv $a ${a/${usource}/$COMPUTERNAME}
done

for a in `ls *${lsource}_???.smx* *${lsource}_???.gpx*`
do
	echo "renaming $a to ${a/${lsource}/${COMPUTERNAME}}"
	mv $a ${a/${lsource}/$COMPUTERNAME}
done

for a in `ls *.smi`
do
	echo $a
	sed "s/${usource}/${COMPUTERNAME}/g;s/${lsource}/${COMPUTERNAME}/g" ${a} > ${a}.new
	if [ ! -s ${a}.new ]
	then
		echo "error in substitute of $a file, exiting ..."
		exit 1
	fi
	mv ${a} ${a}.bak
	mv ${a}.new ${a}
done
