use Win32::OLE;
use OLE;

unless (@ARGV == 6) {
	die("usage: createCompanySitemaps SERVER DATABASE USER PASSWORD SRVNAME TYPE");
}
$SERVER=$ARGV[0];
$DATABASE=$ARGV[1];;
$USER=$ARGV[2];
$PASSWORD=$ARGV[3];
my $SRVNAME=$ARGV[4];
$TYPE=$ARGV[5];

my $freq;
my $counter=0;
$fieldNum=4;
$priority=0.5;
$PRI_BATCH=5000;
$QUERY = "select sn.xlsid, master.dbo.RemovePunctAndSpaces(master.dbo.Anglicize(sn.name)), convert(varchar(10), CASE WHEN last_update < 'June 17, 2008' THEN 'June 17, 2008' ELSE last_update END,120) ";
$DELQUERY = "delete sitemaps where dbase='search' and regkey='co' and server='$SRVNAME' ";
if ($TYPE eq "public") {
	$QUERY .= ", convert(varchar(8),getdate(),120) + '01' ";
	$QUERY .= " from snapshot1 sn with (NOLOCK) ";
	$QUERY .= " where (sn.is_not_alacra is NULL or sn.is_not_alacra = 9958) ";
	$QUERY .= " and sn.public_private_flag = 1";
	$QUERY .= " order by sn.mktcap desc ";
	$counter=1;
	$freq="weekly";
	$priority=1.0;
	$DELQUERY .= " and part < 100";
} elsif ($TYPE eq "private") {
	$QUERY .= ", convert(varchar(5),getdate(),120) + '01-01' ";
	$QUERY .= " from snapshot1 sn with (NOLOCK) ";
	$QUERY .= " where (sn.is_not_alacra is NULL or sn.is_not_alacra = 9958) ";
	$QUERY .= " and sn.public_private_flag is null ";
	$QUERY .= " order by sn.name ";
	$counter=101;
	$freq="monthly";
	$priority=0.5;
	$DELQUERY .= " and part > 100";
	$PRI_BATCH=-1;
} else {
	print "Incorrect Type provided";
	exit 1;
}

print ("$QUERY \n");

#exit 0;

my $SITEMAPFILE;
unless (open(SITEMAPFILE, ">sitemap.sql")){
	die "cannot open sitemap.sql file";
}

my $OUTFILE;
&openFile();

#Create connection strings
my $conn=CreateObject OLE "ADODB.Connection" || die "CreateObject: $!";
my $connect="DRIVER={SQL SERVER};PWD=$PASSWORD;UID=$USER;SERVER=$SERVER;DATABASE=$DATABASE";

#Connect
$conn->open($connect);

#Execute query
my $sql=$QUERY;

my $rs;

unless($rs=$conn->Execute($sql)){
  die("error executing the SQL statement");
}


$MAX_BATCH=48000;
$total_count=0;
$batch_count=0;
$pri_count=0;
#Get values
$myfldcnt = $rs->Fields->Count;
if ($myfldcnt eq $fieldNum ){
	print SITEMAPFILE ("$DELQUERY\n");
	while ( !$rs->EOF){
		$total_count++;
		$batch_count++;
		$pri_count++;
		if ($batch_count > $MAX_BATCH) {
			&closeFile(1, $batch_count-1);
			$batch_count=1;
			$counter++;
			&openFile();
		}
		if ($PRI_BATCH > 0 && $pri_count > $PRI_BATCH) {
			$pri_count=1;
			$priority-=0.1;
		}
		if ($priority < 0.1) {
			$priority=0.1;
		}
		$id = $rs->Fields(0)->value;
		$name = $rs->Fields(1)->value;
		# use translate spaces to underscores
#		$name =~ tr/ /_/;
		$date = $rs->Fields(2)->value;
		$date2 = $rs->Fields(3)->value;
		if ($date eq "" || $date2 gt $date) {
			$date=$date2
		}

		&writeSet($id, $name, $date, $priority);
		$rs->MoveNext;
	} 
	print("total records: $total_count\n");
} else {
	print ("number of fields is not equal to $fieldNum\n"); 
	exit 1;
}

&closeFile(0, $batch_count-1);
$rs->Close;
$conn->Close;

sub closeFile{
	local ($gzip, $batch) = @_;
	print OUTFILE ("</urlset>\n");
	close (OUTFILE);
	$file=sprintf("search_co_%s_%03d.smx", $SRVNAME, $counter);
	if ($gzip eq 1 ) {
		system("gzip -f $file");
	}
	print SITEMAPFILE ("insert into sitemaps(dbase, regkey, server, part, archived, indextime, lastmod, doc_count, sitemap_size, metadata_size)\n");
	print SITEMAPFILE ("values('search', 'co', '$SRVNAME', $counter, $gzip, getdate(), getdate(), $batch, 0, 0)\n ");


}

sub openFile {
	$file=sprintf("search_co_%s_%03d.smx", $SRVNAME, $counter);
	unless (open(OUTFILE, ">$file")){
		die "cannot open $file file";
	}
print ("$file\n");
	print OUTFILE ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
	print OUTFILE ("<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n");
}

sub writeSet {
	local ($xlsid, $stopname, $date, $priority) = @_ ;
	print OUTFILE ("<url>\n");
	print OUTFILE ("<loc>http://www.alacrastore.com/company-snapshot/$stopname-$xlsid</loc>\n");
	print OUTFILE ("<lastmod>$date</lastmod>\n");
	print OUTFILE ("<changefreq>$freq</changefreq>\n");
	print OUTFILE ("<priority>$priority</priority>\n");
	print OUTFILE ("</url>\n");
}
