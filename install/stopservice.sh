if [ "$XLSUTILS" == "" ]
then
	XLSUTILS=$XLS/src/scripts/loading/dba
fi

. $XLSUTILS/get_registry_value.fn

shname=$0

# Parse the command line arguments.
#	1 = servicecode
#	2 = checkerror

if [ $# -lt 1 ]
then
	echo "Usage: ${shname} servicecode [checkerror]"
	exit 1
fi

# Save the runstring parameters in local variables
servicecode=$1
if [ $# -gt 1 ]
then
	checkerror=$2
else
	checkerror=0
fi


stopservice()
{
	if [ $# -lt 2 ]
	then
		return 1
	fi
	sname=$1
	errcheck=$2

	echo "Stopping ${sname}"
	net stop "${sname}"
	if [ $? != 0 ]
	then
		echo "Error stopping ${sname}!"
		if [ $errcheck != 0 ]
		then
			exit 1
		fi
	fi
}


if [ "${servicecode}" = "availability" ]
then
	if [ "$COMPUTERNAME" != "" ]
	then
			dba2server=`get_xls_registry_value dba2 "Server" "0"`
			echo "Marking $COMPUTERNAME offline"
			isql /b /U dba2 /P dba2 /S ${dba2server} /Q "exec setServerStatus '$COMPUTERNAME', 'offline'"
			if [ $? -ne 0 ]; then echo "error taking machine offline"; exit 1; fi
	fi

	stopservice "Alacra Server Availability Service" "${checkerror}"
	sleep 70
	exit 0
fi


if [ "${servicecode}" = "skvalidservice" ]
then
	# dnb service has skvalidservice in its service dependency list
	stopservice "Alacra Company DNB Service" 0
	stopservice "XLS User Validation Services" "${checkerror}"
	stopservice "XLS User Validation Services - Local DB" "${checkerror}"
	exit 0
fi


if [ "${servicecode}" = "companyinfoservice" ]
then
	stopservice "Alacra Company Information Service" "${checkerror}"
	exit 0
fi


if [ "${servicecode}" = "dnb2service" ]
then
	stopservice "Alacra Company DNB Service" "${checkerror}"
	exit 0
fi


if [ "${servicecode}" = "apidatasource" ]
then
	stopservice "Alacra APIDataSource" "${checkerror}"
	exit 0
fi


if [ "${servicecode}" = "appserver" ]
then
	stopservice "Alacra Application Server Update Service" "${checkerror}"
	exit 0
fi


if [ "${servicecode}" = "alachartsservice" ]
then
	stopservice "Alacra Charting Service" "${checkerror}"
	exit 0
fi


if [ "${servicecode}" = "creditcardservice" ]
then
	stopservice "Alacra Credit Card Authentication Service" "${checkerror}"
	exit 0
fi


if [ "${servicecode}" = "encryptionservice" ]
then
	stopservice "Alacra CreditCard Encryption Service" "${checkerror}"
	exit 0
fi


if [ "${servicecode}" = "alacralogloader" ]
then
	stopservice "Alacra Log Loading Service" "${checkerror}"
	exit 0
fi


if [ "${servicecode}" = "dispatch" ]
then
	stopservice "XLS Dispatch Service" "${checkerror}"
	exit 0
fi


if [ "${servicecode}" = "unlinktmp" ]
then
	stopservice "XLS Unlink Temp Service" "${checkerror}"
	exit 0
fi


if [ "${servicecode}" = "www" ]
then
	stopservice "w3svc" "${checkerror}"
	exit 0
fi


if [ "${servicecode}" = "gas" ]
then
	dotnetinstalled=`get_registry_value "HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v3.0" Install`
	dotnetinstalled2=`get_registry_value "HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v3.0" Alacra`
	if [[ "${dotnetinstalled}" -eq "1" || "${dotnetinstalled2}" -eq "1" ]]
	then
#		stopservice "AlacraGAS" "${checkerror}"
		stopservice "Alacra Generic Application Server Service" "${checkerror}"
	else
		echo ".NET 3.0 not detected -- AlacraGAS not installed"
	fi
	exit 0
fi


if [ "${servicecode}" = "autocomplete" ]
then
	stopservice "autocompleteservice" "${checkerror}"
	exit 0
fi


echo "Unknown service code"
exit 1
