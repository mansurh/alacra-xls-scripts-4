# Routine to unpack builds

shname=qunpack.sh

# Parse the command line arguments
#	1 = date YYYYMMDD

if [ $# != 1 ]
then
	echo "Usage: ${shname} YYYYMMDD"
	exit 1
fi

# Save the runstring parameters in local variables
thedate=$1
fullpackname=distribution.data.${thedate}

#echo "Press Enter to unpack distribution file ${disttarzfile}"
#read zz9
unpackpackage.sh ${fullpackname}
exit $?
