#-----------------------------#
# Directions for find_diff.py #
#-----------------------------#

*** Pre-Req - Python 3.4 is installed on the system ***

Note: If you are using a tab delimiter enter "tab" not "\t"

Steps -
1. cd path/to/project/directory
2. ./find_diff.py path/to/fileOne.txt path/to/fileTwo.txt path/to/outputFile.txt "delimiter" "key1|key2|..." "exclude1|exclude2|..."