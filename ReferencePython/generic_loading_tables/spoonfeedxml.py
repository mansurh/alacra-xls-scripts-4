#Spoon Feed XML is for when xml files are too large
#and consist of a large number of relatively small
#children of the root object
#that should cover a large number of cases
#for simplicity this version only works for c6

import codecs
import re
import xml.etree.ElementTree as eTree

re_individual = re.compile(r'<Individual>.*?</Individual>',re.UNICODE|re.DOTALL)
re_company = re.compile(r'<Company>.*?</Company>',re.UNICODE|re.DOTALL)
re_mapper = {'Companies'  :re_company,
             'Individuals':re_individual}

def find_root(f):
    #find <
    while True:
        char = f.read(1) 
        if char != '<' and char !='':
            continue
        else:
            break
    
    Tag=''
    while True:
        char = f.read(1)

        #write tag name until > or end of file is found 
        if char == '':
            raise Exception("xml root not found")
        if char != '>':
            Tag += char
        else:
            break
     
    return Tag


def get_entities(xmlstring,regex):
    
    #print(xmlstring)
    matches = regex.finditer(xmlstring)
    string = ''
    lastindex=0
    for m in matches:
        string+=m.group(0)
        lastindex = m.end(0)
    #print(matches)
    if lastindex > -1:
        remainder = xmlstring[lastindex:]
    else:
        remainder = xmlstring
    
    return string,remainder


def xml_spoon_feeder(file):
    global re_mapper

    print("Processing file: " + file)
    with codecs.open(file, 'rb', encoding='utf16') as xml_file:
        Root = find_root(xml_file)

        if Root == "Companies/" or Root == "Individuals/":#check for no records
            return

        remainder = ''
        regex = re_mapper[Root]
        while True:
            CHUNK_SIZE = 40000000
            raw_chunk = xml_file.read(CHUNK_SIZE)

            if len(raw_chunk) == 0:
                break

            root_children,remainder = get_entities(remainder + raw_chunk,regex)
                
            if root_children == '':
                continue
                
            xml_piece = '<{0}>{1}</{0}>'.format(Root,root_children)

            yield xml_piece
                
