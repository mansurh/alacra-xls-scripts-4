from db_retriever import DB_Retriever
from file_hand import File_Hand
import argparse
import sys
import os
import time
from log_me import logger

def main():
	# Command line parsing
	def msg(name='ipid_load'):
		return "python " + name + ".py TODO msg()"
	
	# Parser for user argument input.
	parser = argparse.ArgumentParser(prog='ipid_load', usage=msg())
	
	# Client directory argument used to locate the files needed to BCP data into the destination table
	parser.add_argument('client_dir', type=str)
	
	# Destination is the name of the directory under Data Servers in the registry with the credentials
	# for the database you want to create the table in.
	parser.add_argument('destination', type=str, nargs='?')
	
	# Test or Prod?
	parser.add_argument('--stage', type=str, metavar='',nargs='?')
	
	# Database server, i.e. datatest4 or data34
	parser.add_argument('--server', type=str, metavar='',nargs='?')
	
	# Max deviation you will allow a new table to have from the previous table without raising a red flag.
	parser.add_argument('--max_dev', type=str, metavar='', nargs='?')
	
	# The name of the BCP file as well as the table name and last part of the create_ file. 
	# Create file should be named like this ... "create_" + file_arg
	parser.add_argument('--file_arg', type=str, metavar='')
	
	# URL of a file to download and use as the bcp file
	parser.add_argument('--url', type=str, metavar='',nargs='?')
	
	# Delimiter in the bcp file
	parser.add_argument('--delimiter', type=str, metavar='', nargs='?')
	
	# First line of file is a column header? Only use this arg if there is a header.
	parser.add_argument('--header', type=bool, metavar='', nargs='?')

	args = parser.parse_args()
	client_dir = args.client_dir
	
	stage = args.stage
	if not stage: stage = 'prod'
	
	# ---------- VARIABLES ---------- #
	scriptdir = os.environ.get('XLS') + '/src/scripts/ReferencePython/generic_loading_tables/'
	loaddir = os.environ.get('XLSDATA') + '/' + client_dir + '/'
	today = time.strftime('%Y-%m-%d')
	# ---------- VARIABLES ---------- #
	
	# Create load directory & sub-directories
	unix_command = 'mkdir -p ' + loaddir + '/log; mkdir -p ' + loaddir + '/bcp_files/' + today
	os.popen(unix_command).read()
	
	# Begin logging
	logging = logger('log.txt',loaddir,loaddir+'log/',stage)
	fh = File_Hand(client_dir, logging)
	
	logging.info("Client directory has been set to: " + client_dir)

	# If destination was defined it is set here.
	destination = args.destination
	# If destination was not defined, default to concordance
	if destination == None: destination = 'concordance' 
	logging.info("Destination has been set to: " + destination)
	
	# Set file_arg here.
	file_arg = args.file_arg
	logging.info("File argument has been set to: " + str(file_arg))
	
	url = None
	if args.url: 
		url = args.url
		unix_command = 'wget -O "'+loaddir+'bcp_files/'+today+'/'+file_arg+'.txt" '+url
		os.popen(unix_command).read()
		
	header = False
	if args.header: header = args.header

	# If max deviation was defined it is set here.
	max_deviation = 20
	# If max deviation was not defined, default to 20%
	if args.max_dev: max_deviation = int(args.max_dev)
	
	delimiter = '|'
	if args.delimiter: delimiter = args.delimiter
	
	file_name = file_arg
	
	# Preparation to create the table (parse the create file).
	columns = fh.get_table_columns(scriptdir+'arg_files/' + client_dir + '/create_' + file_name + '.txt') 
	db_worker = DB_Retriever(destination, columns, '', client_dir, logging) # destination database worker
	creds = db_worker.get_creds("Data Servers")
	server = args.server
	if server:
		creds['dba_server'] = server
	
	logging.info("Establishing a connection with the destination database.")
	# Establish a connection to the destination
	db_conn = db_worker.open_connection(creds)
	db_worker.set_connection(db_conn)
	# Create the new table using the create file.
	db_worker.create_table(file_name, '_new')
	logging.info("Table ("+file_name+"_new) created, not yet populated.")
	
	# BCP the file_arg to the new table in the destination database.
	unix_command = 'bcp ' + destination + '..' + db_worker.myTable +  '_new in ' + loaddir + 'bcp_files/' + today + '/' + file_arg + '.txt -U' + creds['dba_user'] + ' -P' + creds['dba_pass'] + ' -S' + creds['dba_server'] + " -c -t'"+delimiter+"' -CRAW -e "+file_arg+".err"
	if header == True:
		unix_command = 'bcp ' + destination + '..' + db_worker.myTable +  '_new in ' + loaddir + 'bcp_files/' + today + '/' + file_arg + '.txt -U' + creds['dba_user'] + ' -P' + creds['dba_pass'] + ' -S' + creds['dba_server'] + " -F2 -c -t'"+delimiter+"' -CRAW -e "+file_arg+".err"
	os.popen(unix_command).read()
	print(unix_command)
	logging.info("Data has been uploaded to the table via BCP.")
	
	# Check table deviation, if it passes the test rename the tables.
	if db_worker.find_deviation(db_conn, max_deviation):
		print(db_worker.myTable)
		db_worker.rename_tables()
		logging.info("Deviation test has passed, tables have been renamed.")
	else:
		logging.info("Deviation test stopped the system.")
	
	# Check file size, if they pass the test move them to their load directory.
	# This might have to be moved up in the script, kind of useless here?
	if fh.check_file_size('bcp_files/'+today+'/'+file_name+'.txt', 50):
		logging.info("Files have been moved to their load directory.")
	else:
		logging.info('File size check has stopped the system.')
	
	#End logging, sys.exit()
	logging.end_success()


if __name__ == '__main__':
	main()
	
	