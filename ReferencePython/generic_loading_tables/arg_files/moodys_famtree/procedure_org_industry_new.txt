exec moodys_org_industry_python_proc
---------------------------
ALTER PROCEDURE moodys_org_industry_python_proc    
AS            
BEGIN            
        
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[org_industry_python_table]') AND type in (N'U'))        
DROP TABLE [dbo].[org_industry_python_table]            
      
create table org_industry_python_table (    
org_id varchar(20),
ticker varchar(255),
name varchar(255),
country varchar(255),
broad_business_line varchar(4096),
specific_business_line varchar(20),
specific_SIC varchar(20),
specific_SIC_text varchar(4096),
broad_SIC varchar(20),
broad_SIC_text varchar(4096),
industry varchar(4096),
PRIMARY KEY (org_id)
)  
            
INSERT INTO org_industry_python_table
SELECT DISTINCT
org.Organization_ID, 
CASE WHEN ident.ID_Type_Text='Exchange Ticker' 
THEN ident.Organization_ID_Value 
END AS ticker,
org.Moodys_Legal_Name, org.Domicile_Country_Name,
mark.Broad_Business_Line_Text, mark.Specific_Business_Line_Code,
mark.Specific_SIC_Industry_Code, mark.Specific_SIC_Industry_Text,
mark.Broad_SIC_Industry_Code, mark.Broad_SIC_Industry_Text,
mark.Diversity_SIC_Text
FROM CFG_moodysload_organization_rating_root org
INNER JOIN CFG_moodysload_organization_market mark
ON org.Organization_ID=mark.Organization_ID
LEFT OUTER JOIN 
(select Organization_ID, ID_Type_Text, Organization_ID_Value from CFG_moodysload_organization_identifiers
where ID_Type_Text='Exchange Ticker') ident
ON org.Organization_ID=ident.Organization_ID
ORDER BY org.Organization_ID


CREATE INDEX index1 ON  org_industry_python_table (name);  



END