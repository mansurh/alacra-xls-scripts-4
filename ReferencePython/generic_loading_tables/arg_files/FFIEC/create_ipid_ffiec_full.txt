id_rssd|int
d_dt_start|varchar(50)
d_dt_end|varchar(50)
bhc_ind|int
broad_reg_cd|int
chtr_auth_cd|int
chtr_type_cd|int
fbo_4c9_ind|int
fhc_ind|int
func_reg|int
insur_pri_cd|int
mbr_fhlbs_ind|int
mbr_frs_ind|int
sec_rptg_status|int
est_type_cd|int
bank_cnt|int
bnk_type_analys_cd|int
d_dt_exist_cmnc|varchar(50)
d_dt_exist_term|varchar(50)
fisc_yrend_mmdd|decimal(4,0)
d_dt_insur|varchar(50)
d_dt_open|varchar(50)
fncl_sub_holder|int
fncl_sub_ind|int
iba_grndfthr_ind|int
ibf_ind|int
id_rssd_hd_off|int
mjr_own_mnrty|int
nm_lgl|varchar(120)
nm_short|varchar(30)
nm_srch_cd|int
org_type_cd|int
reason_term_cd|int
cnsrvtr_cd|int
entity_type|varchar(4)
auth_reg_dist_frs|int
act_prim_cd|varchar(6)
city|varchar(25)
cntry_nm|varchar(40)
id_cusip|varchar(15)
state_abbr_nm|varchar(2)
place_cd|int
state_cd|int
state_home_cd|int
street_line1|varchar(40)
street_line2|varchar(40)
zip_cd|varchar(9)
id_thrift|int
id_thrift_hc|varchar(6)
domestic_ind|varchar(1)
id_aba_prim|int
id_fdic_cert|int
id_ncua|int
county_cd|int
dist_frs|int
id_occ|int
cntry_cd|decimal(5,0)
dt_end|int
dt_exist_cmnc|int
dt_exist_term|int
dt_insur|int
dt_open|int
dt_start|int
ID_TAX|int
PROV_REGION|varchar(40)
URL|varchar(255)
SLHC_IND|int
SLHC_TYPE_IND|int
PRIM_FED_REG|varchar(20)
STATE_INC_CD|int
CNTRY_INC_CD|int
STATE_INC_ABBR_NM|varchar(2)
CNTRY_INC_NM|varchar(40)
