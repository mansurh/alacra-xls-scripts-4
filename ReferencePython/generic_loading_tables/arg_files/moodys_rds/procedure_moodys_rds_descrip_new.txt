exec procedure moodys_rds_descrip_python_proc
------------------------------------------------------
Create PROCEDURE [dbo].[moodys_rds_descrip_python_proc]
AS          
BEGIN          
      
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[moodys_rds_history_python_table]') AND type in (N'U'))      
DROP TABLE [dbo].[moodys_rds_history_python_table]          
    
create table moodys_rds_history_python_table (    
moody_id varchar(50),
debt_id int,
issuer_id int,
rating varchar(4096),
watchlist_date varchar(50),
expr_date varchar(50),
watchlist_exp_date varchar(50),
rating_action varchar(4096),
rating_direction varchar(255),
watchlist_indicator varchar(40),
watchlist_reason varchar(4096),
indicator varchar(4096)
)    

insert into moodys_rds_history_python_table (moody_id, debt_id, issuer_id, rating, watchlist_date, 
expr_date, watchlist_exp_date, rating_action, rating_direction, watchlist_reason, indicator)
select cmir.Moodys_Rating_ID, cmirr.Instrument_ID, cmio.Organization_ID, cmir.Rating_Text,
cmir.Rating_Local_Date, cmir.Rating_Termination_Local_Date, cmiw.Watchlist_Local_Termination_Date,
cmir.Rating_Reason_Text, cmir.Rating_Direction_Short_Description, cmiw.Watchlist_Direction_Text,
cmira.Rating_Attribute_Text
from CFG_moodysload_instrument_ratings cmir
join CFG_moodysload_instrument_rating_root cmirr on cmirr.Instrument_ID = cmir.Instrument_ID
join CFG_moodysload_instrument_organization cmio on cmio.Instrument_ID = cmir.Instrument_ID
join CFG_moodysload_instrument_watchlist cmiw on cmiw.Instrument_ID = cmir.Instrument_ID
join CFG_moodysload_instrument_ratings_attribute cmira on cmira.Instrument_ID = cmir.Instrument_ID

END