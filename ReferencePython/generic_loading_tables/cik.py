import argparse
import sys
import os
import time
import subprocess
import io
import cik_full
from datetime import timedelta, date

def main():
	# Command line parsing.
	def msg(name='cik.py'):
		return "python " + name + ".py TODO msg()"
	# Parser for user argument input.
	parser = argparse.ArgumentParser(prog='cik', usage=msg())
	
	# The destination argument is used to define the directory where files will be downloaded to.
	# $XLSDATA/destination
	parser.add_argument('--destination', type=str, metavar='',nargs='?')
	parser.add_argument('--frequency', type=str, metavar='',nargs='?')

	args = parser.parse_args()
	
	# If the user provided a destination argument it is set here.
	destination = args.destination 
	# If the user did not define a destination argument it is set to 'cik'.
	if destination == None: destination = 'cik'	
	
	frequency = args.frequency
	if frequency == None: frequency = 'daily'
	
	
	# ---------- VARIABLES ---------- #
	XLSDATA = os.environ.get('XLSDATA') + '/'
	XLS = os.environ.get('XLS') + '/' 
	num_days_back = 1
	yesterday = (date.today() - timedelta(days=num_days_back)).strftime('%Y%m%d')
	today = date.today().strftime('%Y-%m-%d')
	month = date.today().month
	year = date.today().year
	quarters = {1: 'QTR1', 2: 'QTR1', 3: 'QTR1', 
				4: 'QTR2', 5: 'QTR2', 6: 'QTR2', 
				7: 'QTR3', 8: 'QTR3', 9: 'QTR3', 
				10: 'QTR4', 11: 'QTR4', 12: 'QTR4'}
	# ---------- VARIABLES ---------- #
	
	# Create destination directory.
	unix_command = 'mkdir -p ' + XLSDATA + destination + '/data_files/' + today
	os.popen(unix_command)
	unix_command = 'mkdir -p ' + XLSDATA + destination + '/bcp_files/' + today 
	os.popen(unix_command)
	
	# Clean up the destination directory from previous times the script has been run.
	unix_command = "cd " + XLSDATA + destination + '/data_files/' + today + "; rm -f *.idx"
	os.popen(unix_command)
	unix_command = "cd " + XLSDATA + destination + '/bcp_files/' + today + "; rm -f *.txt"
	os.popen(unix_command)
	
	if frequency != 'daily':
		base_url = 'https://www.sec.gov/Archives/edgar/full-index/'
		url = base_url + str(year) + '/' + quarters[month] + '/master.idx'
	
		# Set the working directory
		os.chdir(XLSDATA + destination + '/data_files/' + today)	
		unix_command = 'cd ' + XLSDATA + destination + '/data_files/' + today
		print(unix_command)
		os.popen(unix_command)

		# Download the file
		unix_command = 'wget "' + url + '"'
		print(unix_command)
		url_status = subprocess.Popen(unix_command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
		stdout_value = url_status.communicate()[0].decode("utf-8")
		print(stdout_value)
	
		if 'ERROR 404' in stdout_value:
			print("File at " + url + " could not be found")
			sys.exit(1)

		# Parse the file so the data is in correct bcp format
		with open(XLSDATA+destination + '/data_files/' + str(today) + '/master.idx', 'r') as masterfile:
			emptyHeader = 0
			while not emptyHeader:
				headerData = masterfile.readline()
				if headerData != '\n' and headerData == ((len(headerData) - 1) * '-') + '\n': # if we are at the end of the line, all the characters are '-' and we know its the divider
					break 
			with open(XLSDATA+destination + '/bcp_files/' + str(today) + '/cik_master_load.txt', 'w') as newmasterfile:
				documentEnd = 0
				while not documentEnd:
					readData = masterfile.readline().strip('\n')
					if not readData:
						documentEnd = 1
						break
					readData = readData + "\r\n"
					newmasterfile.write(readData)
				newmasterfile.close()
			masterfile.close()
			unix_command = 'cp ' +XLSDATA+destination + '/bcp_files/' + str(today) + '/cik_master_load' + '.txt '
			unix_command = unix_command + XLS+'src/scripts/referencepython/generic_loading_tables/arg_files/cik/' 
			os.popen(unix_command)
			return

	# Fetch yesterday's master .txt file.
	base_url = 'https://www.sec.gov/Archives/edgar/daily-index/'
	url = base_url + str(year) + '/' + quarters[month] + '/master.' + yesterday + '.idx'
	
	# Set the working directory
	os.chdir(XLSDATA + destination + '/data_files/' + today)
	unix_command = 'cd ' + XLSDATA + destination + '/data_files/' + today
	print(unix_command)
	os.popen(unix_command)

	# Download the file
	unix_command = 'wget "' + url + '"'
	print(unix_command)
	url_status = subprocess.Popen(unix_command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
	stdout_value = url_status.communicate()[0].decode("utf-8")
	print(stdout_value)

	if 'ERROR 404' in stdout_value:
		print("File at " + url + " could not be found")
		sys.exit(1)

	# Parse the file so the data is in correct bcp format
	with open(XLSDATA + destination + '/data_files/' + today + '/master.' + yesterday + '.idx', 'r') as masterfile:
		emptyHeader = 0
		while not emptyHeader:
			headerData = masterfile.readline()
			if headerData == ((len(headerData) - 1) * '-') + '\n': # if we are at the end of the line, all the characters are '-' and we know its the divider
				break 
		with open(XLSDATA+destination + '/bcp_files/' + today +'/cik_master_load' + '.txt', 'w') as newmasterfile:
			documentEnd = 0
			while not documentEnd:
				readData = masterfile.readline().strip('\n')
				if not readData:
					documentEnd = 1
					break
				readData = readData + "\r\n"
				newmasterfile.write(readData)
			newmasterfile.close()
		masterfile.close()
		unix_command = 'cp ' +XLSDATA+destination + '/bcp_files/' + today + '/cik_master_load' + '.txt '
		unix_command = unix_command + XLS+'src/scripts/referencepython/generic_loading_tables/arg_files/cik/' 
		os.popen(unix_command)

'''	
# Excess code for the company and the form idx files

	# Shows where each of the fields ends
	CONST_COMP_CNAME = 62
	CONST_COMP_FTYPE = 74
	CONST_COMP_CIK = 86
	CONST_COMP_DFILED = 98
	CONST_COMP_FNAME = 150

	CONST_FORM_FTYPE = 12
	CONST_FORM_CNAME = 74
	CONST_FORM_CIK = 86
	CONST_FORM_DFILED = 98
	CONST_FORM_FNAME = 150


	with open('company.' + str(yesterday) + '.idx', 'r') as companyfile:
		emptyHeader = 0
		
		while not emptyHeader: # while we did not clear the top part of index file
			headerData = companyfile.readline()
			print(len(headerData))
			divider = ((len(headerData) - 1) * '-') + '\n'
			if headerData == divider: # if we are at the end of the line, all the characters are '-' and we know its the divider
				print('checking')
				break
				
		with open('company.' + str(yesterday) + '.txt', 'w') as newcompanyfile:
			companyEnd = 0			
			while not companyEnd: # while there is still more to read
				readData = companyfile.readline()
				if not readData.strip():
					companyEnd = 1
					break
				company = readData[:CONST_COMP_CNAME].rstrip()
				formtype = readData[CONST_COMP_CNAME:CONST_COMP_FTYPE].rstrip()
				CIK = readData[CONST_COMP_FTYPE:CONST_COMP_CIK].rstrip()
				datefiled = readData[CONST_COMP_CIK:CONST_COMP_DFILED].rstrip()
				filename = readData[CONST_COMP_DFILED:CONST_COMP_FNAME].rstrip()			
				newcompanyfile.write(company + '|' + formtype + '|' + CIK + '|' + datefiled + '|' + filename + '\n')
			newcompanyfile.close()
		companyfile.close()

	
	try:
		with open('form.' + str(yesterday) + '.idx', 'r') as formfile:
			emptyHeader = 0
			while not emptyHeader:
				headerData = formfile.readline()
				i = 0
				for c in headerData:
					if c != '-':
						break
					else:
						i += 1
				if i == len(headerData):
					emptyHeader = 1
			with open('form.' + str(yesterday) + '.txt', 'w') as newformfile:
				documentEnd = 0		
				while not documentEnd:
					readData = formfile.readline()
					if not readData:
						documentEnd = 1
						break
					filetype = readData[:CONST_FORM_FTYPE].rstrip()
					companyname = readData[CONST_FORM_FTYPE:CONST_FORM_CNAME].rstrip()
					CIK = readData[CONST_FORM_CNAME:CONST_FORM_CIK].rstrip()
					datefiled = readData[CONST_FORM_CIK:CONST_FORM_DFILED].rstrip()
					filename = readData[CONST_FORM_DFILED:CONST_FORM_FNAME].rstrip()			
					newformfile.write(filetype + '|' + companyname + '|' + CIK + '|' + datefiled + '|' + filename)
				newformfile.close()
			formfile.close()
	except:
		print("Could not find file " + 'form.' + str(yesterday) + '.idx')
	'''	
if __name__ == '__main__':
	main()
	