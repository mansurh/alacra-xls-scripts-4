from lib.DatabaseWorker import DatabaseWorker
from lib.FTP_Doctor import FTP_Doctor
from lib.API_Wizard import API_Wizard
import logging
import sys

""" Main function for DatabaseWorker.py & FTP_Doctor.py"""
def main():
	#initialize logging file name
	logging.basicConfig(filename='logger.log', filemode='w', level=logging.DEBUG, 
	format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p: ')
	
	#start database ONLY work 
	database = input('Would you like to use the concordance database? (y/n): ')
	if database == 'y': # user answers yes
		database = 'concordance'
		databaseWorker = DatabaseWorker(database)
	elif database == 'n': # user answers no
		database = input('Enter the name of the database you want to use: ')
		databaseWorker = DatabaseWorker(database)
	else: # user provides invalid answer, restart main()
		main()	
	logging.info('Retrieving credentials for ' + database + '.')
	creds = databaseWorker.get_creds('Data Servers')
	logging.info('Establishing a connection to ' + database + '.')
	db_conn = databaseWorker.open_connection(creds)
	logging.info('Connection established on ' + database+ '.')
	choice = input('What would you like to do:\n'
					'1. Create a new table\n'
					'2. Update an existing table\n'
					"Enter '1' or '2': ")
	source = input('Enter the client name (i.e. mpitcher): ')
	logging.info('Client name set to: "' + source + '"')
	if choice == '1':
		logging.info('Choice #1 ... creating a table named ipid_' + source + '.')
		databaseWorker.create_table(db_conn, source, '')
		logging.info('Creation of ipid_' + source + ' complete.')
	elif choice == '2':
		logging.info('Choice #2 ... Updating a table named ipid_' + source + '.')
		logging.info('Creating a table named ipid_' + source + '_new.')
		databaseWorker.create_table(db_conn, source, '_new')
		logging.info('Creation of ipid_' + source + '_new complete.')
	regDir = databaseWorker.find_ipid_client_data(db_conn, 'regDir')
	ftp_databaseWorker = DatabaseWorker(regDir)
	logging.info('Retrieving credentials for ' + regDir + '.')
	ftp_creds = ftp_databaseWorker.get_creds('File Servers')
	file_name = databaseWorker.find_ipid_client_data(db_conn, 'ftpDir')
	#end database ONLY work
	
	ftp_doctor = FTP_Doctor(source, file_name)
	logging.info('Opening FTP connection.')
	ftp_conn = ftp_doctor.open_connection(ftp_creds['dba_server'], ftp_creds['dba_user'], ftp_creds['dba_pass'])
	logging.info('FTP connection is open, retrieving data...')
	ftp_doctor.retrieve_data(ftp_conn)
	logging.info('Data retrieved, checking file size...')
	threshold = databaseWorker.find_ipid_client_data(db_conn, 'threshold')
	ftp_doctor.check_file_size(ftp_doctor.file_name, threshold)
	
	#start database AND ftp work
	logging.info('File size has been checked, loading data into the table...')
	delimiter = databaseWorker.find_ipid_client_data(db_conn, 'delimiter')
	ftp_doctor.load_data(database, creds, databaseWorker.table_name, delimiter)
	if choice == '2':
		max_deviation = databaseWorker.find_ipid_client_data(db_conn, 'deviationAllowed')
		logging.info('Checking table deviation, max deviation=' + str(max_deviation) + '.')
		ftp_doctor.find_deviation(db_conn, source, max_deviation)
		logging.info('Deviation has been checked, renaming the tables...')
		databaseWorker.rename_tables(db_conn)
		logging.info('Tables have been renamed. Update finished.')
	#end database AND ftp work
	
	extract_data = databaseWorker.find_ipid_client_data(db_conn, 'extractData')
	args = {'one':1,'two':2,'three':3}
	api_wizard = API_Wizard(source, extract_data, args)
	api_wizard._extract_data()
	
	#closing connections
	db_conn.close()
	logging.info('Database: ' + database + ' - connection has been closed.')
	ftp_conn.close()
	logging.info('FTP: ' + regDir + ' - connection has been closed.')
	
	#add another table?
	answer = input('Would you like to create or update another table? (y/n): ')
	if answer == 'y':
		logging.info('Creating or updating another table.')
		main()
	elif answer == 'n':
		logging.info('Finished session.')
		sys.exit()
	else:
		logging.info('Invalid answer given: ' + answer + '. Exiting system.')
		print('Invalid answer given: "' + answer + '" ... Exiting system.')
		sys.exit()
	#start logger email
	
	#install blat v2.2.2!!
	#logger = Logger()
	#logger.send_mail()
	'''msg = MIMEMultipart()
	msg['Subject'] = 'Logging Information Attached'
	msg['From'] = 'matt.e.pitcher@gmail.com'
	msg['To'] = 'rosa.sac@alacra.com'
	part = MIMEText('See attachment for logged information...')
	msg.attach(part)
	part = MIMEApplication(open('logger.log'),'rb').read()
	part.add_header('Content-Disposition', 'attachment', filename='logger.log')
	msg.attach(part)
	smtp = SMTP('smtp.domain.cu')
	smtp.ehlo()
	smtp.login('matt.e.pitcher@gmail.com', 'password')
	
	smtp.sendmail(msg['From'], msg['To'], msg.as_string())'''
	#end logger email
	
	

if __name__ == '__main__':
	main()