import pyodbc
import gspread
from oauth2client.service_account import ServiceAccountCredentials

def main():
	server = 'datatest4'
	database = 'concordance'
	user = 'concordance'
	pwd = 'concordance'
	conn = pyodbc.connect('DRIVER=SQL Server;SERVER={0};DATABASE={1};UID={2};PWD={3};'.format(server,database,user,pwd), autocommit=True)
	cursor = conn.cursor()
	#--Entities rated by
	sql = 'select count(*) from snapshot1 where is_rated = 1'
	ERB = cursor.execute(sql).fetchone()[0]
	#--Entities listed on
	sql = 'select count(*) from snapshot1 where is_listed = 1'
	ELO = cursor.execute(sql).fetchone()[0]
	#-- Exchanges around the world
	sql = 'select count(distinct exchange) from security'
	EAW = cursor.execute(sql).fetchone()[0]
	#--LEIs assigned by 
	sql = 'select count(*) from snapshot1 where is_lei = 1'
	LEIs = cursor.execute(sql).fetchone()[0]
	#--LOUs
	sql = 'select count(distinct registrationmanagingLOU) from ipid_gleif'
	LOUs = cursor.execute(sql).fetchone()[0]
	
	scope = "https://spreadsheets.google.com/feeds"
	credentials = ServiceAccountCredentials.from_json_keyfile_name('credentials.json', scope)
	gs = gspread.authorize(credentials)		
	
	gsheet = gs.open("ELO_EAW")	
	wsheet = gsheet.worksheet("Sheet1")
	wsheet.update_acell('A2',ELO)
	wsheet.update_acell('A3',EAW)
	
	gsheet = gs.open("Entities_CR")	
	wsheet = gsheet.worksheet("Sheet1")
	wsheet.update_acell('A2',ERB)
	
	gsheet = gs.open("LEI_LOU")	
	wsheet = gsheet.worksheet("Sheet1")
	wsheet.update_acell('A2',LEIs)
	wsheet.update_acell('A3',LOUs)
	#data = wsheet.range('A1:A2')[0]
	#print(data)
	
	
if __name__ == '__main__':
	main()