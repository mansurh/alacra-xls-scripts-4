#!/bin/sh
today=`date +%a`

weekend=0

case $today in
Sat)  weekend=1;;
Sun)  weekend=1;;
esac

if [ $weekend = "1" ]
then 
    exit 0
fi

localdir="/export/home/kkaebnick"
logdir="/export/home/kkaebnick/temp"
export localdir
export logdir
fdfnfile="fdfnlog`date +%m%d`.log"

echo "FTP start (`date`)" > $logdir/fdfnlog.log
ftp -n ftp.analystcall.com <$localdir/fdfn.ftp 2>$logdir/error.log

if [ -s $logdir/error.log ]
then
    echo "Error, fdfn ftp failed at `date`" >> $logdir/fdfnlog.log
    cat $logdir/error.log >> $logdir/fdfnlog.log
    exit 1
fi

echo "FTP complete (`date`)" >> $logdir/fdfnlog.log
# rm -f $localdir/*.d*
cp $logdir/fdfnlog.log  /export/home/kkaebnick/$fdfnfile

#rm -f $logdir/*.log

exit 0

