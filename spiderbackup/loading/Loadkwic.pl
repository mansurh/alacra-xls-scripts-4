use Win32::OLE;
use OLE;

$SERVER=$ARGV[0];
$DATABASE=$ARGV[1];;
$USER=$ARGV[2];
$PASSWORD=$ARGV[3];
$datadir=$ARGV[4];
$QUERY=$ARGV[5];
$file=$ARGV[6];

print ("query=$QUERY \n");

unless (open(OUTFILE, ">$datadir/$file")){
   die "cannot open $datadir/$file file";
}

#Create connection strings
my $conn=CreateObject OLE "ADODB.Connection" || die "CreateObject: $!";
my $connect="DRIVER={SQL SERVER};PWD=$PASSWORD;UID=$USER;SERVER=$SERVER;DATABASE=$DATABASE";

#Connect
$conn->open($connect);

$conn->{CommandTimeout} = 180; 

#Execute query
my $sql=$QUERY;

my $rs;
unless($rs=$conn->Execute($sql)){
  die("error executing the SQL statement");
}

$index=0;
$total_count=0;
#Get values
$myfldcnt = $rs->Fields->Count;
#print("number $myfldcnt\n");

if ($myfldcnt == 2 ){
  while ( !$rs->EOF){
      $total_count++;
      $id = $rs->Fields(0)->value;
      $name = $rs->Fields(1)->value;

      # use translate operator to make upper case
      $name =~ tr/a-z/A-Z/;
      # strip leading & trailing spaces from the name
      $name =~ s/^ +//;
      $name =~ s/ +$//;

      if ($name eq "") {
        print "Skipping record with no name: $id\n";
        $rs->MoveNext;
        next;
      }

      #output the full name of the company
      print OUTFILE ("$id|$name\n");
      $last= $name;
      $last =~ s/[\.\,;:\|\(\)\'\"%#\*\$\?=@\[\]]+/ /g;

      $last =~ s/^ +//;
      $last =~ s/ +$//;
      $last =~ s/ +/ /g;
      #print punctiationless name
      if ( $name ne $last && $last ne "" ) {
         print OUTFILE ("$id|$last\n");
      }

      $last2 = $last;
      $last2 =~ s/[\-\+&\\\/]+/ /g;
      $last2 =~ s/^ +//;
      $last2 =~ s/ +$//;
      $last2 =~ s/ +/ /g;
#      if ( $last2 ne $last) {
#         print ("$id|$last2\n");
#      }

      $last2 = $last;
      #$last =~ s/^THE //;
      if ( $last ne $last2) {
#         print ("$id|$last\n");
         $last2 = $last;
      }
      $last =~ s/ A\/S$| A S$| AB$| ADR$| AG$| AKA$| ASA$| AS$| BHD$| BV$| COMPANY$| CO$| CORPORATION$| CORP$| CV$| DE$| FKA$| GMBH$| III$| II$| INCORPORATED$| INC$| IV$| I$| KFT$|K K$| KG$| KK$| LDA$| LIMITED$| LLC$| LP$| LTD$| MBH$| N V$| NV$| OF$| PLC$| PTE$| PTY$| S A$| S P A$| SA$| SDN$| SPA$| SP$| SRL$| SRO$| THE$| USA$| VI$| ZOO$//g;

      if ( length($last2) > length($last)) {
	    $tail = substr($last2, length($last)+1);
      } else {
	    $tail = "";
      }
      @parts=split(/ +/,$last );
      
      $countp=0;
      while ( $countp < @parts ){
          $index=0;
          $line="";
          if (length($parts[$countp]) > 1) {
             while ( $index < @parts-$countp ){
               $line .= $parts[$index+$countp];
               $line .= " ";
               $index++;
             }

             $line .= $tail;
             $line =~ s/ $//g;
             if ($last2 ne $line  && $line ne "" ) {
                print OUTFILE ("$id|$line\n");
             }
          }
          $countp++;
      }
      @parts=split(/[\-\+&\\\/]+/,$last );
      $countp=0;
      $line="";
      while ( $countp < @parts && @parts > 1){
          $index=0;
          $line="";
          if (length($parts[$countp]) > 1) {
             while ( $index < @parts-$countp ){
                $line .= $parts[$index+$countp];
                $line .= " ";
                $index++;
             }
             $line =~ s/ +/ /g;
             $line =~ s/^ +//;

             $line .= $tail;
             $line =~ s/ $//g;
             if ($name ne $line  && $line ne "" )
             {
                print OUTFILE ("$id|$line\n");
             }
          }
          $countp++;
      }
    $rs->MoveNext;
  } 
  print("total records: $total_count\n");
} else {
  print ("number of fields is not equal to 2\n"); 
}

$rs->Close;
end:
$conn->Close;


close (OUTFILE);
