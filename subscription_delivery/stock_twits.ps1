
# Stock Twits SubscriptionDeliveryService script

[Reflection.Assembly]::LoadWithPartialName("System.ServiceModel.Syndication") | Out-Null
[Reflection.Assembly]::LoadWithPartialName("System.Xml.Linq") | Out-Null
[Reflection.Assembly]::LoadWithPartialName("System.Web") | Out-Null

[Reflection.Assembly]::LoadWithPartialName("Alacra.Util") | Out-Null

. 'c:/users/xls/src/scripts/utility/try_catch.ps1'
. 'c:/users/xls/src/scripts/utility/usingobj.ps1'
. 'c:/users/xls/src/scripts/utility/shortenurl_bitly.ps1'


$paramStockTwitUrl = [Alacra.Util.General]::GetXLSRegistryValue("XLS/Application Options/AlacraSubscriptionDeliveryService","StockTwitsPubUrl",'http://api.stocktwits.com/api/messages/create.json')
$paramStockTwitApiKey = [Alacra.Util.General]::GetXLSRegistryValue("XLS/Application Options/AlacraSubscriptionDeliveryService","StockTwitsApiKey",'1499fc6a0737beafb6847e0f72ac4f9b7263ec68')
$StockTwitsAnalystRecommendationStreamId = [Alacra.Util.General]::GetXLSRegistryValue("XLS/Application Options/AlacraSubscriptionDeliveryService","StockTwitsAnalystRecommendationStreamId",'9')
$StockTwitsDealRumorStreamId = [Alacra.Util.General]::GetXLSRegistryValue("XLS/Application Options/AlacraSubscriptionDeliveryService","StockTwitsDealRumorStreamId",'11')
$StockTwitsAnnouncedDealStreamId = [Alacra.Util.General]::GetXLSRegistryValue("XLS/Application Options/AlacraSubscriptionDeliveryService","StockTwitsAnnouncedDealStreamId",'10')

$paramStockTwitSubscriptionId = ''
$eventGuid = ''

	$twiterr = New-Object System.Text.StringBuilder
	foreach ($item in $rss.GetEnumerator()) {
		$twit = ""
		$itemel = [System.Xml.Linq.XElement]::Parse($item.Value)

		$guidel = $itemel.Element([System.Xml.Linq.XName]::Get("guid"))
		if ($guid -ne $null) {
			$eventGuid = $guidel.Value
		}
		$linkel = $itemel.Element([System.Xml.Linq.XName]::Get("link"))
		if ($linkel -eq $null) {
			$twiterr.AppendFormat("Missing rss link element {0}", $eventGuid)
			continue
		}

		$updnel = $itemel.Element([System.Xml.Linq.XName]::Get("{http://www.alacra.com/pulse}upgrade_downgrade"))
		$coel = $itemel.Element([System.Xml.Linq.XName]::Get("{http://www.alacra.com/pulse}company_name"))
		$tickerel = $itemel.Element([System.Xml.Linq.XName]::Get("{http://www.alacra.com/pulse}company_ticker"))
		$firmel = $itemel.Element([System.Xml.Linq.XName]::Get("{http://www.alacra.com/pulse}firm_name"))
		$dealstatusel = $itemel.Element([System.Xml.Linq.XName]::Get("{http://www.alacra.com/pulse}deal_status_id"))
		$dealtitleel = $itemel.Element([System.Xml.Linq.XName]::Get("{http://www.alacra.com/pulse}deal_name"))
		$targettickerel = $itemel.Element([System.Xml.Linq.XName]::Get("{http://www.alacra.com/pulse}target_ticker"))
		$acquirertickerel = $itemel.Element([System.Xml.Linq.XName]::Get("{http://www.alacra.com/pulse}acquirer_ticker"))
		if ($updnel -ne $null -and $updnel.Value.Length -gt 0) {
			if ($firmel -eq $null) {
				$twiterr.AppendFormat("Missing pulse:firm_name element {0}", $eventGuid)
			} elseif ($tickerel -eq $null) {
				$twiterr.AppendFormat("Missing pulse:company_ticker element {0}", $eventGuid)
			} elseif ($coel -eq $null) {
				$twiterr.AppendFormat("Missing pulse:company_name element {0}", $eventGuid)
			} else {

				$prevratingel = $itemel.Element([System.Xml.Linq.XName]::Get("{http://www.alacra.com/pulse}previousRating"))
				$nextratingel = $itemel.Element([System.Xml.Linq.XName]::Get("{http://www.alacra.com/pulse}nextRating"))
				$tofromstr = ""
				if (($nextratingel -ne $null -and $nextratingel.Value.Length -gt 0) -and ($prevratingel -eq $null -or $prevratingel.Value.Length -eq 0)) {
					$tofromstr = [string]::Format("to {0} ", $nextratingel.Value)
				} elseif (($nextratingel -ne $null -and $nextratingel.Value.Length -gt 0) -and ($prevratingel -ne $null -and $prevratingel.Value.Length -gt 0)) {
					$tofromstr = [string]::Format("to {0} from {1} ", $nextratingel.Value, $prevratingel.Value)
				}

				$shorturl = ShortenUrl $linkel.Value

				if ($updnel.Value.CompareTo("1") -eq 0) {
					$updntxt = "upgrades"
				} elseif ($updnel.Value.CompareTo("2") -eq 0) {
					$updntxt = "downgrades"
				} else {
					$updntxt = "reiterates"
				}
				$paramStockTwitSubscriptionId = $StockTwitsAnalystRecommendationStreamId
					
				if (-not [string]::IsNullOrEmpty($shorturl)) {  # don't tweet when we didn't get a short url
					$tickstr = ''
					if ($tickerel.Value.Length -gt 0) {
						$tickstr = [string]::Format("${0}", $tickerel.Value)
					}
					$twit = [string]::Format("{0} {1} {2} {3}{4} {5}", $firmel.Value, $updntxt, $coel.Value, $tofromstr, $shorturl.ToString(), $tickstr)
				} else {
					throw "no short url retrieved"
				}

			}
		} elseif ($dealstatusel -ne $null -and ($dealstatusel.Value.CompareTo("1") -eq 0 -or $dealstatusel.Value.CompareTo("7") -eq 0 -or $dealstatusel.Value.CompareTo("8") -eq 0)) {

			if ($dealtitleel -eq $null) {
				$twiterr.AppendFormat("Missing pulse:deal_name element {0}", $eventGuid)
			} else {
				$targetticker = ""
				if ($targettickerel -ne $null) {
					$targetticker = [string]::Format("${0}", $targettickerel.Value)
				}

				$acquirerticker = ""
				if ($acquirertickerel -ne $null) {
					$acquirerticker = [string]::Format("${0}", $acquirertickerel.Value)
				}

				$shorturl = ShortenUrl $linkel.Value
				$paramStockTwitSubscriptionId = $StockTwitsDealRumorStreamId
				$spacerstr = ''
				if ($targetticker.Length -gt 0 -and $acquirerticker.Length -gt 0) {
					$spacerstr = " "
				}
				if (-not [string]::IsNullOrEmpty($shorturl)) {  # don't tweet when we didn't get a short url
					$twit = [string]::Format("{0} {1} {2}{3}{4}", $dealtitleel.Value, $shorturl, $targetticker, $spacerstr, $acquirerticker)
				} else {
					throw "no short url retrieved"
				}
			}
		} elseif ($dealstatusel -ne $null -and ($dealstatusel.Value.CompareTo("3") -eq 0 -or $dealstatusel.Value.CompareTo("4") -eq 0)) {

			if ($dealtitleel -eq $null) {
				$twiterr.AppendFormat("Missing pulse:deal_name element {0}", $eventGuid)
			} else {
				$targetticker = ""
				if ($targettickerel -ne $null) {
					$targetticker = [string]::Format("${0}", $targettickerel.Value)
				}

				$acquirerticker = ""
				if ($acquirertickerel -ne $null) {
					$acquirerticker = [string]::Format("${0}", $acquirertickerel.Value)
				}

				$shorturl = ShortenUrl $linkel.Value
				$paramStockTwitSubscriptionId = $StockTwitsAnnouncedDealStreamId
				$spacerstr = ''
				if ($targetticker.Length -gt 0 -and $acquirerticker.Length -gt 0) {
					$spacerstr = " "
				}
				if (-not [string]::IsNullOrEmpty($shorturl)) {  # don't tweet when we didn't get a short url
					$twit = [string]::Format("{0} {1} {2}{3}{4}", $dealtitleel.Value, $shorturl, $targetticker, $spacerstr, $acquirerticker)
				} else {
					throw "no short url retrieved"
				}
			}
		} else {
			$twiterr.AppendFormat("Unhandled item {0}", $eventGuid)
		}

		if (-not [string]::IsNullOrEmpty($twit)) {
			$postData = New-Object System.Text.StringBuilder
			$postData.AppendFormat("body={0}&", [System.Web.HttpUtility]::UrlEncode($twit))
			$postData.AppendFormat("api_key={0}&", $paramStockTwitApiKey)
			$postData.AppendFormat("stream_subscription_ids={0}", $paramStockTwitSubscriptionId)
			Write-Output [string]::Format("stocktwit req: {0}", $postData.ToString())

			$utf8postData = [System.Text.Encoding.UTF8]::GetBytes($postData.ToString())
			$req = [System.Net.WebRequest]::Create($paramStockTwitUrl)
			$req.Method = "POST"
			$req.ContentType = "application/x-www-form-urlencoded"
			$postStream = $req.GetRequestStream()
			$postStream.Write($utf8postData, 0, $utf8postData.Length)
			$postStream.Close()
			$resp = $req.GetResponse()
			UsingObj ($sr = New-Object System.IO.StreamReader($resp.GetResponseStream())) {
				$jsonresp = $sr.ReadToEnd()
				if (-not $jsonresp.Contains('"status":200')) {
					throw [string]::Format("No status 200 found! {0} ({1})", $jsonresp, $twit)
				}
				Write-Output [string]::Format("stocktwits resp: {0}", $jsonresp)
			}
		}
	}

	if ($twiterr.Length -gt 0) {
		throw $twiterr.ToString()
	}
