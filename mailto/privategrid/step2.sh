# step2.sh 
if [ $# -lt 8 ]
then
	exit 1
fi

server=$1
login=$2
password=$3
sqlfile=$4
okmail=$5
ngmail=$6
email=$7
uuid=$8

isql /S ${server} /U ${login} /P ${password} < ${sqlfile} > ${sqlfile}.out

grep -s "\*\*\*DUPLICATE\*\*\*" ${sqlfile}.out
if [ $? = 0 ]
then
	c:/usr/local/bin/blat.exe ${ngmail} -t "${email}" -f "Administrators@PrivateGrid.com" -s "Your PrivateGrid Membership Request" -q
else
	c:/usr/local/bin/blat.exe ${okmail} -t "${email}" -f "Administrators@PrivateGrid.com" -s "Your PrivateGrid Membership Request" -q
fi

# rm ${sqlfile}.out

exit 0


#if [ $# -lt 20 ]
#then
#    exit 1
#fi

#id=$1
#companyname=$2
#companydomain=$3
#address1=$4
#address2=$5
#city=$6
#state=$7
#zip=$8
#country=$9
#contactname=$10
#contactphone=$11
#contactemail=$12
#contactcallback=$13
#ipaddress=$14
#ipisp=$15
#ipemailserver=$16
#ipdnsserver=$17
#billingmethod=$18
#userlogin=$19
#userpassword=$20

#isql /S bruni /U mailto /P mailto << HERE
#INSERT application 
#(id,companyname,companydomain,address1,address2,city,state,zip,country,contactname,contactphone,contactemail,contactcallback,ipaddress,ipisp,ipemailserver,ipdnsserver,billingmethod,userlogin,userpassword)
#VALUES
#('${id}','${companyname}','${companydomain}','${address1}','${address2}','${city}','${state}','${zip}','${country}','${contactname}','${contactphone}','${contactemail}','${contactcallback}','${ipaddress}','${ipisp}','${ipemailserver}','${ipdnsserver}','${billingmethod}','${userlogin}','${userpassword}')
#GO
#HERE
