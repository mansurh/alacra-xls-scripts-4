TITLEBAR="BNI Index Update"

dataserver=`hostname`

save_backup()
{
  mkdir -p backup
  cp -p bni* backup/
  cp -p rds* backup/
}


# Mark the database as updating
$XLS/src/scripts/loading/dba/startupdate.sh rds ${dataserver} rds
if [ $? -ne 0 ]
then
	echo "error in startupdate.sh rds ${dataserver} rds"
	exit 1
fi

cd g:/rdsindex/
# Delete any old copies of the index
$XLS/src/scripts/utility/rmInvFileSet.sh bni
$XLS/src/scripts/utility/rmInvFileSet.sh rds

# Now rebuild the index - part 1
#newrdsindex.exe -d bni -t 10000000 -m 200000000 -S "December 31, 1994"
if [ $? -ne 0 ]
then
	echo "Error updating news indices, Exiting"
	exit 1
fi

# Continue to rebuild the rest
for yeartoindex in `isql /S${dataserver} /Urds /Prds -h-1 -n /Q "set nocount on select distinct datepart(year, publication_date) from story where publication_date >= '2001-01-01' and datepart(year, publication_date) < datepart(year, getdate()) order by datepart(year, publication_date)"`
do
  save_backup
  echo "newrdsindex.exe -d bni -t 10000000 -m 200000000 -s January 1, ${yeartoindex} -S June 30, ${yeartoindex}"
  newrdsindex.exe -d bni -t 10000000 -m 200000000 -s "January 1, ${yeartoindex}" -S "June 30, ${yeartoindex}"
  if [ $? -ne 0 ]
  then
	echo "Error updating news indices for first half of $yeartoindex , Exiting"
	exit 1
  fi
  echo "newrdsindex.exe -d bni -t 10000000 -m 200000000 -s July 1, ${yeartoindex} -S December 31, ${yeartoindex}"
  newrdsindex.exe -d bni -t 10000000 -m 200000000 -s "July 1, ${yeartoindex}" -S "December 31, ${yeartoindex}"
  if [ $? -ne 0 ]
  then
	echo "Error updating news indices for second half of $yeartoindex , Exiting"
	exit 1
  fi
done

save_backup
yeartoindex=`date +%Y`
echo "newrdsindex.exe -d bni -t 10000000 -m 200000000 -s January 1, ${yeartoindex} -S June 30, ${yeartoindex}"
newrdsindex.exe -d bni -t 10000000 -m 200000000 -s "January 1, ${yeartoindex}" -S "June 30, ${yeartoindex}"
if [ $? -ne 0 ]
then
	echo "Error updating news indices for first half of $yeartoindex , Exiting"
	exit 1
fi

morestories=`isql /S${dataserver} /Urds /Prds -h-1 -n /Q "set nocount on select count(*) from story where publication_date >= '${yeartoindex}-07-01'"`
if [ ${morestories} -gt 0 ]
then
  save_backup
  echo "newrdsindex.exe -d bni -t 10000000 -m 200000000 -s July 1, ${yeartoindex} -S December 31, ${yeartoindex}"
  newrdsindex.exe -d bni -t 10000000 -m 200000000 -s "July 1, ${yeartoindex}" -S "December 31, ${yeartoindex}"
  if [ $? -ne 0 ]
  then
	echo "Error updating news indices for second half of $yeartoindex , Exiting"
	exit 1
  fi
fi

rm -r -f backup
# Mark the database update as complete
$XLS/src/scripts/loading/dba/endupdate.sh rds ${dataserver} rds
