TITLEBAR="BAMP Index Update"

dataserver=`hostname`

# where is the index located?
currentloc=`reginvidx get bamp`

scratch=${currentloc%/*}
indexdir=${scratch}
cd ${indexdir}
if [ $? -ne 0 ]
then
	echo "Index directory not found, exiting"
	exit 1
fi

# Mark the database as updating
$XLS/src/scripts/loading/dba/startupdate.sh bamp ${dataserver} bamp
if [ $? -ne 0 ]
then
	echo "error in startupdate.sh bamp ${dataserver} bamp"
	exit 1
fi

# Save old copies of the index
$XLS/src/scripts/utility/moveInvFileSet.sh bamp .
if [ $? -ne 0 ]
then
	echo "error saving old version of the index, exiting"
	exit 1
fi
#rm -r -f backup
#mkdir -p backup
#mv bamp* backup/
#cp -p $XLS/src/cfgfiles/bamp.cfg .

# Now rebuild the index - part 1
newrdsindex.exe -d bamp -t 4000000 -m 110000000 -S "December 31, 1998"
if [ $? -ne 0 ]
then
	echo "Error updating news indices, Exiting"
	exit 1
fi

# Continue to rebuild the index - part 2
newrdsindex.exe -d bamp -t 4000000 -m 110000000 -s "January 1, 1999" -S "December 31, 2001"
if [ $? -ne 0 ]
then
	echo "Error updating news indices, Exiting"
	exit 1
fi

# Continue to rebuild the index - part3 
newrdsindex.exe -d bamp -t 4000000 -m 110000000 -s "January 1, 2002" -S "December 31, 2003" 
if [ $? -ne 0 ]
then
	echo "Error updating news indices, Exiting"
	exit 1
fi

# Continue to rebuild the index - part4 
newrdsindex.exe -d bamp -t 4000000 -m 110000000 -s "January 1, 2004"  
if [ $? -ne 0 ]
then
	echo "Error updating news indices, Exiting"
	exit 1
fi
# Mark the database update as complete
$XLS/src/scripts/loading/dba/endupdate.sh bamp ${dataserver} bamp
