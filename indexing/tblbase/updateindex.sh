TITLEBAR="Tablebase Index Update"

dataserver=`hostname`

# where is the index located?
currentloc=`reginvidx get tblbase`

scratch=${currentloc%/*}
indexdir=${scratch}
cd ${indexdir}
if [ $? -ne 0 ]
then
	echo "Index directory not found, exiting"
	exit 1
fi

# Mark the database as updating
$XLS/src/scripts/loading/dba/startupdate.sh tblbase ${dataserver} tblbase
if [ $? -ne 0 ]
then
	echo "error in startupdate.sh tblbase ${dataserver} tblbase"
	exit 1
fi

# Save old copies of the index
$XLS/src/scripts/utility/moveInvFileSet.sh tblbase .
if [ $? -ne 0 ]
then
	echo "error saving old version of the index, exiting"
	exit 1
fi
#rm -r -f backup
#mkdir -p backup
#mv tblbase* backup/
#cp -p $XLS/src/cfgfiles/tblbase.cfg .

# Now rebuild the index
newrdsindex.exe -d tblbase -t 4000000 -m 60000000
if [ $? -ne 0 ]
then
	echo "Error updating news indices, Exiting"
	exit 1
fi

# Mark the database update as complete
$XLS/src/scripts/loading/dba/endupdate.sh tblbase ${dataserver} tblbase
